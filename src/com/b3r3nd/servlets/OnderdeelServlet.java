package com.b3r3nd.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.b3r3nd.reference.Bedrijf;

public class OnderdeelServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    	String dispatch = null;

        Bedrijf b = (Bedrijf) this.getServletContext().getAttribute("Bedrijf");

        String artikelnummer = (String)request.getParameter("artikelnummer");
        String prijs = (String)request.getParameter("prijs");
        String type = (String)request.getParameter("type");
        
        boolean bb = false;
    
   	 if(!artikelnummer.equals("") && !prijs.equals("") && !type.equals("")) {
   	
   		 int iArtikelnummer = Integer.parseInt(artikelnummer);
   		 int iPrijs = Integer.parseInt(prijs);
   		 
      	b.getVoorraadbeheer().addOnderdeel(iArtikelnummer, 0, iPrijs, type);
      	bb = true;
      } else {
      	request.setAttribute("fout", "Please fill all fields");
      }
        
        if(bb) {
        	dispatch = "/secure/vooraad/vooraad.jsp";
        } else {
        	dispatch = "/secure/vooraad/newonderdeel.jsp";
        }
        	request.getRequestDispatcher(dispatch).forward(request, response);
    }
}
