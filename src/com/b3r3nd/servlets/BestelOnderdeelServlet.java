package com.b3r3nd.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.b3r3nd.domein.agenda.Reparatie;
import com.b3r3nd.domein.voorraad.Onderdeel;
import com.b3r3nd.reference.Bedrijf;

public class BestelOnderdeelServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    	String dispatch = null;

        Bedrijf b = (Bedrijf) this.getServletContext().getAttribute("Bedrijf");

        String onderdeel = request.getParameter("onderdeel");
        String sAantal = request.getParameter("aantal");
        
        if(!onderdeel.equals("") && !sAantal.equals("")) {
        	int aantal = Integer.parseInt(sAantal);
            Onderdeel o = b.getVoorraadbeheer().zoekOnderdeel(onderdeel);
            o.setAantal(o.getAantal() + aantal);
            dispatch = "/secure/vooraad/vooraad.jsp";
        } else {
        	 dispatch = "/secure/vooraad/bestelonderdelen.jsp";
        	 request.setAttribute("fout", "Vul alle velden in");
        }
       
        request.getRequestDispatcher(dispatch).forward(request, response);
    }
}
