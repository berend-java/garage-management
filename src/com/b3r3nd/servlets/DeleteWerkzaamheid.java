package com.b3r3nd.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.b3r3nd.domein.agenda.Reparatie;
import com.b3r3nd.domein.agenda.Werkzaamheid;
import com.b3r3nd.reference.Bedrijf;

public class DeleteWerkzaamheid extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    	String dispatch = null;

        Bedrijf b = (Bedrijf) this.getServletContext().getAttribute("Bedrijf");

        String werk = (String)request.getParameter("werkzaamheid");
        
        Werkzaamheid w = b.getWerkplaats().zoekWerkzaamheid(werk);
        b.getWerkplaats().removeWerkzaamheid(w);
        
        dispatch = "/secure/agenda/werkzaamheden.jsp";
        request.getRequestDispatcher(dispatch).forward(request, response);
    }
}
