package com.b3r3nd.servlets;

import java.io.IOException;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.b3r3nd.domein.agenda.Werkzaamheid;
import com.b3r3nd.reference.Bedrijf;


public class WerkzaamheidServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    	String dispatch = null;

        Bedrijf b = (Bedrijf) this.getServletContext().getAttribute("Bedrijf");
        boolean bb = false;
        String omschrijving = (String)request.getParameter("omschrijving");
        
   	 if(!omschrijving.equals("")) {
   		Werkzaamheid w = new Werkzaamheid(omschrijving, new Date());
   		w.setGereed(true);
      	b.getWerkplaats().addWerkzaamheid(w);
      	bb = true;
      } else {
      	request.setAttribute("fout", "Vul alle velden in");
      }
        
        if(bb) {
        	dispatch = "/secure/agenda/werkzaamheden.jsp";
        } else {
        	dispatch = "/secure/agenda/newwerkzaamheid.jsp";
        }
        	request.getRequestDispatcher(dispatch).forward(request, response);
    }
}
