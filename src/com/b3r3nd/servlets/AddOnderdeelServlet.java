package com.b3r3nd.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.b3r3nd.domein.agenda.Reparatie;
import com.b3r3nd.domein.voorraad.Onderdeel;
import com.b3r3nd.reference.Bedrijf;

public class AddOnderdeelServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    	String dispatch = null;

        Bedrijf b = (Bedrijf) this.getServletContext().getAttribute("Bedrijf");

        String reparatie = request.getParameter("reparatie");
        String onderdeel = request.getParameter("onderdeel");
        String sAantal = request.getParameter("aantal");
        
        if(!reparatie.equals("") && !onderdeel.equals("") && !sAantal.equals("")) {
        	int aantal = Integer.parseInt(sAantal);
        	Reparatie r = b.getWerkplaats().zoekReparatie(reparatie);
            Onderdeel o = b.getVoorraadbeheer().zoekOnderdeel(onderdeel);
            
            if(o.getAantal() >= aantal) {
            	Onderdeel o2 = new Onderdeel();
            	o2.setAantal(aantal);
            	o2.setArtikelnummer(o.getArtikelnummer());
            	o2.setPrijs(o.getPrijs());
            	o2.setType(o.getType());
            	r.addOnderdeel(o2);
            	 
            	 o.setAantal(o.getAantal() - aantal);
            	 dispatch = "/secure/agenda/reparaties.jsp";
            } else {
            	 dispatch = "/secure/agenda/addonderdelen.jsp";
            	 request.setAttribute("fout", "Huidig ondeel niet in voorraad");
            }
        } else {
        	 dispatch = "/secure/agenda/addonderdelen.jsp";
        	 request.setAttribute("fout", "Vul alle velden in");
        }
       
        request.getRequestDispatcher(dispatch).forward(request, response);
    }
}
