package com.b3r3nd.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.b3r3nd.domein.agenda.Reparatie;
import com.b3r3nd.reference.Bedrijf;


public class EditReparatieServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    	String dispatch = null;

        Bedrijf b = (Bedrijf) this.getServletContext().getAttribute("Bedrijf");
        
        String isGereed = (String)request.getParameter("isgereed");
        String status = (String)request.getParameter("status");
        String reparatie = (String)request.getParameter("reparatie");
        Reparatie r = null;
        boolean bb = false;
        
   	 	if(!isGereed.equals("") && !status.equals("") && !reparatie.equals("")) {
   	 		r = b.getWerkplaats().zoekReparatie(reparatie);
   	 		r.setStatus(status);
   	 		if(isGereed.equals("true")) {
   	 			r.setIsGereed(true);
   	 		} else { 
   	 			r.setIsGereed(false); 
   	 		}
   	 		bb = true;
   	 	} else {
   	 		request.setAttribute("fout", "Vul alle velden in");
   	 	}

        dispatch = "/secure/agenda/reparatie.jsp?reparatie="+r.getNaam();
        request.getRequestDispatcher(dispatch).forward(request, response);
    }
}
