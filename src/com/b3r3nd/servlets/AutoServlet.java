package com.b3r3nd.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import com.b3r3nd.domein.garage.Auto;
import com.b3r3nd.domein.klantenbinding.Klant;
import com.b3r3nd.reference.Bedrijf;


public class AutoServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    	String dispatch = null;

        Bedrijf b = (Bedrijf) this.getServletContext().getAttribute("Bedrijf");

        String kenteken = (String)request.getParameter("kenteken");
        String type = (String)request.getParameter("type");
        String merk = (String)request.getParameter("merk");
        String brandstof = (String)request.getParameter("brandstof");
        String klant = (String)request.getParameter("klant");
        
        boolean bb = false;

       if(!kenteken.equals("") && !type.equals("") && !merk.equals("") && !brandstof.equals("") && !klant.equals("")) {
        	Auto a = new Auto(type, merk, brandstof, kenteken);
        	Klant k = b.getKlantenbinding().zoekKlant(klant);
        	a.setDeEigenaar(k);
        	b.getParkeergarage().addAuto2(a);
        	bb = true;
      } else {
      	request.setAttribute("fout", "Please fill all fields");
      }
        
        if(bb) {
        	dispatch = "/secure/garage/autos.jsp";
        } else {
        	dispatch = "/secure/garage/newauto.jsp";
        }
        	request.getRequestDispatcher(dispatch).forward(request, response);
    }
}
