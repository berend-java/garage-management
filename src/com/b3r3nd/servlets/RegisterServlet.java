package com.b3r3nd.servlets;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.b3r3nd.domein.klantenbinding.Klant;
import com.b3r3nd.reference.Bedrijf;

public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Bedrijf b = (Bedrijf) this.getServletContext().getAttribute("Bedrijf");
		
		String username = req.getParameter("username");
		String voornaam = req.getParameter("voornaam");
		String achternaam = req.getParameter("achternaam");
		String password = req.getParameter("password");
		String password2 = req.getParameter("password2");
		String email = req.getParameter("email");
		String email2 = req.getParameter("email2");
		String geb = req.getParameter("geb");
		
		String foutString = "";
		boolean fout = false;
		RequestDispatcher rd = null;
		
		if(username.equals("") || voornaam.equals("") || password.equals("") || email.equals("") || achternaam.equals("") ||  email2.equals("") || password2.equals("") || geb.equals("")) {
			foutString = "Vul aub alle velden in";
			fout = true;
		} else if(!password.equals(password2)) {
			foutString = "Wachtwoorden zijn niet gelijk";
			fout = true;
		} else if(!email.equals(email2)) {
			foutString = "Emails zijn niet gelijk";
			fout = true;
		} else {
			//String voornaam, String achternaam, String email, Date geboortedatum
			  Date d = null;
			try { d = new SimpleDateFormat("dd-mm-yyyy").parse(geb);
			} catch (ParseException e) { e.printStackTrace(); }
			
			Klant k = new Klant(voornaam, achternaam, email, d);
			k.setUsername(username);
			k.setRegistered(true);
			k.setPassword(password);
			
			b.getKlantenbinding().addKlant(k);
		}
			

		if(fout) {
			rd = req.getRequestDispatcher("register.jsp");
			req.setAttribute("fout", foutString);
		} else {
			rd = req.getRequestDispatcher("index.jsp");
		}
		
		rd.forward(req, resp);
	}
}
