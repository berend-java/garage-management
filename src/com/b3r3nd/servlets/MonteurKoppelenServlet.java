package com.b3r3nd.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.b3r3nd.domein.agenda.Monteur;
import com.b3r3nd.domein.agenda.Reparatie;
import com.b3r3nd.reference.Bedrijf;

public class MonteurKoppelenServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    	String dispatch = null;
        Bedrijf b = (Bedrijf) this.getServletContext().getAttribute("Bedrijf");

        String monteur = (String)request.getParameter("monteur");
        String reparatie = (String)request.getParameter("reparatie");
        
        boolean bb = false;
    
   	 if(!monteur.equals("") && !reparatie.equals("")) {
   		 Monteur m = b.getWerkplaats().zoekMonteur(monteur);
      	 Reparatie r = b.getWerkplaats().zoekReparatie(reparatie);
      	 
      	 r.addMonteur(m);
   		 
      	bb = true;
      } else {
      	request.setAttribute("fout", "Please fill all fields");
      }
        
        if(bb) {
        	dispatch = "/secure/agenda/reparaties.jsp";
        } else {
        	dispatch = "/secure/agenda/addmonteur.jsp";
        	request.setAttribute("fout", "Vul alle velden in");
        }
        	request.getRequestDispatcher(dispatch).forward(request, response);
    }
}
