package com.b3r3nd.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import com.b3r3nd.domein.agenda.Monteur;
import com.b3r3nd.reference.Bedrijf;


public class MonteurServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    	String dispatch = null;

        Bedrijf b = (Bedrijf) this.getServletContext().getAttribute("Bedrijf");

        String voornaam = (String)request.getParameter("voornaam");
        String achternaam = (String)request.getParameter("achternaam");
        String username = (String)request.getParameter("username");
        String password = (String)request.getParameter("password");
        String geb = (String)request.getParameter("geboortedatum");
        
        boolean bb = false;

       if(!voornaam.equals("") && !achternaam.equals("") && !geb.equals("")) {
    	   Date d = null;
		try { d = new SimpleDateFormat("dd-mm-yyyy").parse(geb);
		} catch (ParseException e) { e.printStackTrace(); }
		
    	   Monteur m = new Monteur(voornaam, achternaam, d);
    	   m.setUsername(username);
    	   m.setPassword(password);
    	   m.setRegistered(true);
    	   b.getWerkplaats().addMonteur(m);
        	bb = true;
      } else {
      	request.setAttribute("fout", "Please fill all fields");
      }
        
        if(bb) {
        	dispatch = "/secure/agenda/monteurs.jsp";
        } else {
        	dispatch = "/secure/agenda/newmonteur.jsp";
        }
        	request.getRequestDispatcher(dispatch).forward(request, response);
    }
}
