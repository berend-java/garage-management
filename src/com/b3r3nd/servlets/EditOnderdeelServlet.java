package com.b3r3nd.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.b3r3nd.domein.voorraad.Onderdeel;
import com.b3r3nd.reference.Bedrijf;

public class EditOnderdeelServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    	String dispatch = null;
    	Onderdeel o = null;
        Bedrijf b = (Bedrijf) this.getServletContext().getAttribute("Bedrijf");

        String artikelnummer = (String)request.getParameter("artikelnummer");
        String prijs = (String)request.getParameter("prijs");
        String type = (String)request.getParameter("type");
        String onderdeel = (String)request.getParameter("onderdeel");
        
        boolean bb = false;
    
   	 if(!artikelnummer.equals("") && !prijs.equals("") && !type.equals("") && !onderdeel.equals("")) {
   		 int iArtikelnummer = Integer.parseInt(artikelnummer);
   		 int iPrijs = Integer.parseInt(prijs);
   		 o = b.getVoorraadbeheer().zoekOnderdeel(onderdeel);
   		 o.setArtikelnummer(iArtikelnummer);
   		 o.setPrijs(iPrijs);
   		 o.setType(type);
      	bb = true;
      } else {
      	request.setAttribute("fout", "vul all velden in");
      }
        
        if(bb) {
        	dispatch = "/secure/vooraad/vooraad.jsp";
        } else {
        	dispatch = "/secure/vooraad/editonderdeel.jsp?"+o.getType();
        }
        	request.getRequestDispatcher(dispatch).forward(request, response);
    }
}
