package com.b3r3nd.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.b3r3nd.domein.klantenbinding.Klant;
import com.b3r3nd.reference.Bedrijf;

public class KlantServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    	String dispatch = null;

        Bedrijf b = (Bedrijf) this.getServletContext().getAttribute("Bedrijf");

        String username = (String)request.getParameter("username");
        String password = (String)request.getParameter("password");
        String voornaam = (String)request.getParameter("voornaam");
        String achternaam = (String)request.getParameter("achternaam");
        String email = (String)request.getParameter("email");
        
        boolean bb = false;
    
   	 if(!username.equals("") && !password.equals("") && !voornaam.equals("") && !achternaam.equals("") && !email.equals("")) {
   		 Klant k = new Klant(voornaam, achternaam, email, null);
   		 k.setUsername(username);
   		 k.setPassword(password);
   		 k.setRegistered(true);
   		 
      	b.getKlantenbinding().addKlant(k);
      	bb = true;
      } else {
      	request.setAttribute("fout", "Please fill all fields");
      }
        
        if(bb) {
        	dispatch = "/secure/klantenbinding/klanten.jsp";
        } else {
        	dispatch = "/secure/klantenbinding/newklant.jsp";
        }
        	request.getRequestDispatcher(dispatch).forward(request, response);
    }
}
