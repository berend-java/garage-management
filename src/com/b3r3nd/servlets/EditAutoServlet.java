package com.b3r3nd.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import com.b3r3nd.domein.garage.Auto;
import com.b3r3nd.domein.klantenbinding.Klant;
import com.b3r3nd.reference.Bedrijf;


public class EditAutoServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    	String dispatch = null;

        Bedrijf b = (Bedrijf) this.getServletContext().getAttribute("Bedrijf");

        String kenteken = (String)request.getParameter("kenteken");
        String type = (String)request.getParameter("type");
        String merk = (String)request.getParameter("merk");
        String brandstof = (String)request.getParameter("brandstof");
        String auto = (String)request.getParameter("auto");
        
        boolean bb = false;

       if(!kenteken.equals("") && !type.equals("") && !merk.equals("") && !brandstof.equals("") && !auto.equals("")) {
    	   Auto a = b.getParkeergarage().zoekAuto(auto);
    	   a.setKenteken(kenteken);
    	   a.setBrandstof(brandstof);
    	   a.setMerk(merk);
    	   a.setType(type);
        	bb = true;
      } else {
      	request.setAttribute("fout", "Vul alle velden in");
      }
        
        if(bb) {
        	dispatch = "/secure/garage/autos.jsp";
        } else {
        	dispatch = "/secure/garage/auto.jsp?auto="+request.getParameter("auto");
        }
        	request.getRequestDispatcher(dispatch).forward(request, response);
    }
}
