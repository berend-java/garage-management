package com.b3r3nd.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.b3r3nd.domein.agenda.Monteur;
import com.b3r3nd.domein.garage.Auto;
import com.b3r3nd.domein.klantenbinding.Klant;
import com.b3r3nd.reference.Bedrijf;
import com.b3r3nd.user.Admin;
import com.b3r3nd.user.GarageBeheerder;
import com.b3r3nd.user.Gebruiker;
import com.b3r3nd.user.UserControl;

public class ReparatieServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    	String dispatch = null;

        Bedrijf b = (Bedrijf) this.getServletContext().getAttribute("Bedrijf");

        String naam = (String)request.getParameter("naam");
        String status = (String)request.getParameter("status");
        String auto = (String)request.getParameter("auto");
        
        Auto a = null;
        
        boolean bb = false;
       
        
        if(!auto.equals("none")) {      	 
        	ArrayList<Auto> autos = b.getParkeergarage().getAutos();	
        	for(Auto aa: autos) {
        		if(aa.getKenteken().equals(auto)) {
        			a = aa;
        		}
        	}
        }
        
   	 if(!naam.equals("") && !status.equals("")) {
      	b.getWerkplaats().addReparatie(status, naam, new Date(), a);
      	bb = true;
      } else {
      	request.setAttribute("fout", "Please fill all fields");
      }
        
        if(bb) {
        	dispatch = "/secure/agenda/reparaties.jsp";
        } else {
        	dispatch = "/secure/agenda/newreparatie.jsp";
        }
        	request.getRequestDispatcher(dispatch).forward(request, response);
    }
}
