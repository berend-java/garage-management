package com.b3r3nd.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.b3r3nd.domein.agenda.Monteur;
import com.b3r3nd.domein.agenda.Reparatie;
import com.b3r3nd.domein.klantenbinding.Klant;
import com.b3r3nd.reference.Bedrijf;

public class DeleteKlantServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    	String dispatch = null;

        Bedrijf b = (Bedrijf) this.getServletContext().getAttribute("Bedrijf");

        String klant = (String)request.getParameter("klant");
        Klant k = b.getKlantenbinding().zoekKlant(klant);
        
        b.getKlantenbinding().removeKlant(k);
        dispatch = "/secure/klantenbinding/klanten.jsp";
        request.getRequestDispatcher(dispatch).forward(request, response);
    }
}
