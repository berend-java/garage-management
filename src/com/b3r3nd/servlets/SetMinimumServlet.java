package com.b3r3nd.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.b3r3nd.domein.voorraad.Onderdeel;
import com.b3r3nd.reference.Bedrijf;

public class SetMinimumServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    	String dispatch = null;
    	Onderdeel o = null;
        Bedrijf b = (Bedrijf) this.getServletContext().getAttribute("Bedrijf");

        String aantal = (String)request.getParameter("aantal");
        String onderdeel = (String)request.getParameter("onderdeel");
        
        boolean bb = false;
    
   	 if(!aantal.equals("") && !onderdeel.equals("")) {

   		 int iAantal = Integer.parseInt(aantal);
   		 o = b.getVoorraadbeheer().zoekOnderdeel(onderdeel);
   		 o.setMinmumAantal(iAantal);
      	bb = true;
      } else {
      	request.setAttribute("fout", "vul all velden in");
      }
        
        if(bb) {
        	dispatch = "/secure/vooraad/vooraad.jsp";
        } else {
        	dispatch = "/secure/vooraad/setminimum.jsp?"+o.getType();
        }
        	request.getRequestDispatcher(dispatch).forward(request, response);
    }
}
