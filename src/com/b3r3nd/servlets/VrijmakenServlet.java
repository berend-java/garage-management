package com.b3r3nd.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.b3r3nd.domein.agenda.Reparatie;
import com.b3r3nd.domein.garage.Plek;
import com.b3r3nd.domein.voorraad.Onderdeel;
import com.b3r3nd.reference.Bedrijf;

public class VrijmakenServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    	String dispatch = null;

        Bedrijf b = (Bedrijf) this.getServletContext().getAttribute("Bedrijf");

        String plek = request.getParameter("plek");
        String schoonmaken = request.getParameter("schoonmaken");
        
        if(!plek.equals("") && !schoonmaken.equals("")) {
        	int iPlek = Integer.parseInt(plek);
        	Plek p = b.getParkeergarage().zoekPlek(iPlek);
        	if(schoonmaken.equals("Ja")) {
        		p.setSchoon(true);
        	} else { p.setSchoon(false); }
        	p.setBezet(false);
        	 dispatch = "/secure/garage/parkeren.jsp";
        } else {
        	  dispatch = "/secure/garage/vrijmaken.jsp";
              request.setAttribute("fout", "Vul alle velden in");
        }     
        request.getRequestDispatcher(dispatch).forward(request, response);
    }
}
