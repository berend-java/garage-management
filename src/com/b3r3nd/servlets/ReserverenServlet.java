package com.b3r3nd.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.b3r3nd.domein.agenda.Reparatie;
import com.b3r3nd.domein.garage.Auto;
import com.b3r3nd.domein.garage.Plek;
import com.b3r3nd.domein.voorraad.Onderdeel;
import com.b3r3nd.reference.Bedrijf;

public class ReserverenServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    	String dispatch = null;

        Bedrijf b = (Bedrijf) this.getServletContext().getAttribute("Bedrijf");

        String datum = request.getParameter("datum");
        String plek = request.getParameter("plek");
        String auto = request.getParameter("auto");
        
        if(!datum.equals("") && !plek.equals("") && !auto.equals("")) {
        	int iPleknummer = Integer.parseInt(plek);
     	   	Date d = null;
     	   	try { d = new SimpleDateFormat("dd-mm-yyyy").parse(datum);
     	   	} catch (ParseException e) { e.printStackTrace(); }
     	   	
     	   	Auto a = b.getParkeergarage().zoekAuto(auto);
        	Plek p = b.getParkeergarage().zoekPlek(iPleknummer);
        	
        	b.getParkeergarage().plekReserveren(a, p, d);
        	dispatch = "/secure/garage/parkeren.jsp";
        	
        } else {
        	request.setAttribute("fout", "Vul alle velden in");
        	dispatch = "/secure/garage/reserveren.jsp";
        }
       
        request.getRequestDispatcher(dispatch).forward(request, response);
    }
}
