package com.b3r3nd.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.b3r3nd.domein.agenda.Monteur;
import com.b3r3nd.domein.agenda.Reparatie;
import com.b3r3nd.domein.agenda.Werkzaamheid;
import com.b3r3nd.reference.Bedrijf;

public class KoppelWerkzaamheidSerlvet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    	String dispatch = null;
        Bedrijf b = (Bedrijf) this.getServletContext().getAttribute("Bedrijf");
        Reparatie r = null;
        String werkzaamheid = (String)request.getParameter("werkzaamheid");
        String reparatie = (String)request.getParameter("reparatie");
        String einddatum = (String)request.getParameter("einddatum");
        
        boolean bb = false;
    
   	 if(!werkzaamheid.equals("") && !reparatie.equals("") && !einddatum.equals("")) {
   		 Werkzaamheid w = b.getWerkplaats().zoekWerkzaamheid(werkzaamheid);
      	 r = b.getWerkplaats().zoekReparatie(reparatie);
      	 Date d = null;
      	 
      	 try { d = new SimpleDateFormat("dd-mm-yyyy").parse(einddatum);
      	 } catch (ParseException e) { e.printStackTrace(); }
      	 
      	 w.setEinddatum(d);
      	 w.setGereed(false);
      	 r.addWerkzaamheid2(w);
      	 bb = true;
   	 }
        if(bb) {
        	dispatch = "/secure/agenda/reparatie.jsp?reparatie="+r.getNaam();
        } else {
        	dispatch = "/secure/agenda/addwerkzaamheid.jsp?reparatie="+r.getNaam();
        	request.setAttribute("fout", "Vul alle velden in");
        }
        	request.getRequestDispatcher(dispatch).forward(request, response);
    }
  }
