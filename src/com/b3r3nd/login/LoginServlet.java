package com.b3r3nd.login;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.b3r3nd.domein.agenda.Monteur;
import com.b3r3nd.domein.klantenbinding.Klant;
import com.b3r3nd.reference.Bedrijf;
import com.b3r3nd.user.Admin;
import com.b3r3nd.user.GarageBeheerder;
import com.b3r3nd.user.Gebruiker;
import com.b3r3nd.user.UserControl;

public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String dispatch = "index.jsp";

        Bedrijf b = (Bedrijf) this.getServletContext().getAttribute("Bedrijf");
        UserControl uc = b.getUserControl();

        HttpSession session = request.getSession();
        String uname = (String) request.getParameter("username");
        String pass = (String) request.getParameter("password");

        boolean unameFound = false;
        for (Gebruiker value : uc.getUsers()) {
            if (value.isRegistered() && value.getUsername().equals(uname)) {
                if (pass.equals(value.getPassword())) {
                    session.setAttribute("username", uname);
                    session.setAttribute("object", value);
                    session.setAttribute("logged", "true");
                    session.setMaxInactiveInterval(600);
                    
                    if(value instanceof Admin){
                        session.setAttribute("userrole", "admin");
                    } else if (value instanceof Klant){
                        session.setAttribute("userrole", "klant");
                    } else if (value instanceof Monteur){
                        session.setAttribute("userrole", "monteur");
                    } else if (value instanceof GarageBeheerder){
                        session.setAttribute("userrole", "garagebeheerder");
                    }
                    dispatch = "secure/welcome.jsp";
                    unameFound = true;
                    
                    Cookie cook = new Cookie("lastUser", value.getUsername());
            		response.addCookie(cook);
                    
                    break;
                } else {
                    break;
                }
            }
        }
        if (!unameFound) {
            request.setAttribute("fout", "Username and/or password incorrect");
            dispatch = "index.jsp";
        }
        request.getRequestDispatcher(dispatch).forward(request, response);
    }
}
