/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.b3r3nd.user;

/**
 * @author Rory
 */
public class GarageBeheerder extends Gebruiker {

    private String voornaam, achternaam;

    public GarageBeheerder(String naam, String achternaam, String username, String pass) {
        this.voornaam = naam;
        this.achternaam = achternaam;
        super.register(username, pass);
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public String getAchternaam() {
        return achternaam;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }
}
