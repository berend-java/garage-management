/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.b3r3nd.user;

import java.io.Serializable;

/**
 * @author Rory
 */
public class Admin extends Gebruiker implements Serializable {
    private String voornaam, achternaam;

    public Admin(String voornaam, String achternaam, String username, String password) {
        this.voornaam = voornaam;
        this.achternaam = achternaam;
        super.register(username, password);
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public String getAchternaam() {
        return achternaam;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }
    
}
