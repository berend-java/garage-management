/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.b3r3nd.user;

import java.io.Serializable;


/**
 * @author Rory
 */
public abstract class Gebruiker implements Serializable {
    private String username, password;
    private boolean registered = false;
    
    public Gebruiker(){};
    
    public boolean register(String uname, String pass) {
        if ("".equals(uname) || "".equals(pass)) {
            return false;
        }

        username = uname;
        password = pass;
        registered = true;

        return true;
    }

    public boolean isRegistered() {
        return registered;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
