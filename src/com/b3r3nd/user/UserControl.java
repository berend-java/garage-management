/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.b3r3nd.user;

import java.io.Serializable;
import java.util.ArrayList;

import com.b3r3nd.domein.agenda.Agenda;

import com.b3r3nd.domein.agenda.Monteur;
import com.b3r3nd.domein.klantenbinding.Klandizie;
import com.b3r3nd.domein.klantenbinding.Klant;

/**
 * @author Rory
 */
public class UserControl implements Serializable {

    private Agenda a;
    private Klandizie k;
    private final ArrayList<Gebruiker> deAdmins = new ArrayList<>();
    private final ArrayList<Gebruiker> deGarageBeheerders = new ArrayList<>();

    public UserControl(Agenda a, Klandizie k) {
        this.a = a;
        this.k = k;
    }

    public ArrayList<Gebruiker> getUsers() {
        ArrayList<Gebruiker> users = new ArrayList<>();

        users.addAll(a.getDeMonteurs());
        users.addAll(k.getDeKlanten());
        users.addAll(deAdmins);
        users.addAll(deGarageBeheerders);

        return users;
    }
    
    public Gebruiker zoekUser(String username) {
    	Gebruiker user = null;
    	for(Gebruiker u: getUsers()) {
    		if(u.getUsername().equals(username)) {
    			user = u;
    		}
    	}
    	return user;
    }

    public void removeUser(Gebruiker u) {
    	if(u instanceof Admin){
            deAdmins.remove(u);
        } else if (u instanceof Klant){
           k.removeKlant((Klant) u);
        } else if (u instanceof Monteur){
            a.removeMonteur((Monteur) u);
        } else if (u instanceof GarageBeheerder){
            deGarageBeheerders.remove(u);
        }
    }
    public boolean addAdmin(String voornaam, String achternaam, String username, String password) {
        if (!getUsers().isEmpty()) {
            for (Gebruiker value : getUsers()) {
                if (value.isRegistered() && value.getUsername().equals(username)) {
                    return false;
                }
            }
        }
        Admin ad = new Admin(voornaam, achternaam, username, password);
        deAdmins.add(ad);
        return true;
    }

    public boolean addGarageBeheerder(String voornaam, String achternaam, String username, String password) {
        if (!getUsers().isEmpty()) {
            for (Gebruiker value : getUsers()) {
                if (value.isRegistered() && value.getUsername().equals(username)) {
                    return false;
                }
            }
        }
        GarageBeheerder gb = new GarageBeheerder(voornaam, achternaam, username, password);
        deGarageBeheerders.add(gb);
        return true;
    }

    public boolean registerKlant(Klant k, String uname, String pass) {
        if (!getUsers().isEmpty()) {
            for (Gebruiker value : getUsers()) {
                if (value.isRegistered() && value.getUsername().equals(uname)) {
                    return false;
                }
            }
        }
        return k.register(uname, pass);
    }

    public boolean registerMonteur(Monteur m, String uname, String pass) {
        if (!getUsers().isEmpty()) {
            for (Gebruiker value : getUsers()) {
                if (value.isRegistered() && value.getUsername().equals(uname)) {
                    return false;
                }
            }
        }
        return m.register(uname, pass);
    }
}
