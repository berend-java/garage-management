package com.b3r3nd.listeners;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SecurityFilter implements Filter {

    @Override
    public void init(FilterConfig fc) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest sr, ServletResponse sr1, FilterChain fc) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)sr;
        HttpServletResponse response = (HttpServletResponse)sr1;
        if (request.getSession().getAttribute("logged") == null) {
            request.getSession().invalidate();
            request.getRequestDispatcher("/index.jsp").forward(sr, sr1);
        } else { 
            fc.doFilter(sr, sr1); 
        }
    }

    @Override
    public void destroy() {
    }

}
