/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.b3r3nd.listeners;

import java.io.IOException;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.b3r3nd.domein.agenda.Monteur;
import com.b3r3nd.domein.klantenbinding.Klant;
import com.b3r3nd.reference.Bedrijf;
import com.b3r3nd.user.GarageBeheerder;

/**
 * @author Rory
 */
public class TestDataServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse respone) throws ServletException, IOException {
        Bedrijf b = (Bedrijf) this.getServletContext().getAttribute("Bedrijf");
        
        Klant k1 = new Klant("Henk", "de Man", "henk@man.de", new Date(1980, 5, 8));
        Klant k2 = new Klant("Piet", "van Test", "piet@test.van", new Date(1985, 8, 2));
        Monteur m1 = new Monteur("Jan", "Kortenaam", new Date(1978, 3, 5));
        
        b.getKlantenbinding().addKlant(k1);
        b.getKlantenbinding().addKlant(k2);
        b.getWerkplaats().addMonteur(m1);
        b.getUserControl().addAdmin("Berend", "de Groot", "Berend", "123");
        b.getUserControl().addGarageBeheerder("Jopie", "Jansen", "JJ", "123");
        
        k1.register("Klant1", "klant");
        k2.register("Klant2", "klant");
        m1.register("Monteur1", "monteur");
        
        getServletContext().setAttribute("Bedrijf", b);
        
        request.getRequestDispatcher("login/login.jsp").forward(request, respone);
    }
}
