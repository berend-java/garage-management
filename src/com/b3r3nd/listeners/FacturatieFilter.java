package com.b3r3nd.listeners;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FacturatieFilter implements Filter {

    @Override
    public void init(FilterConfig fc) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest sr, ServletResponse sr1, FilterChain fc) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)sr;
        HttpServletResponse response = (HttpServletResponse)sr1;
        String s = (String) request.getSession().getAttribute("userrole");
        if (s.equals("admin")) {
        	fc.doFilter(sr, sr1); 
        } else {  
            request.getRequestDispatcher("/secure/welcome.jsp").forward(sr, sr1);
        }
    }

    @Override
    public void destroy() {
    }

}
