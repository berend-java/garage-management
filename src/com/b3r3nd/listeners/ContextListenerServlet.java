package com.b3r3nd.listeners;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.b3r3nd.domein.garage.Auto;
import com.b3r3nd.reference.Bedrijf;

public class ContextListenerServlet implements ServletContextListener {

    public ContextListenerServlet() {}

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        Bedrijf b = new Bedrijf();

        try {
            FileInputStream fis = new FileInputStream("/home/berend/bedrijf.obj");
            ObjectInputStream ois = new ObjectInputStream(fis);

            b = (Bedrijf) ois.readObject();
            ois.close();
            fis.close();
            System.out.println("\n 1 \n");
        } catch (FileNotFoundException ex) {
            b = new Bedrijf();
            System.out.println(" \n 2 \n");
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println("\n 3 \n");
            ex.printStackTrace();
        }
        sce.getServletContext().setAttribute("Bedrijf", b);
        b.getUserControl().addAdmin("Berend", "de Groot", "Berend", "123");
        //b.getParkeergarage().addAuto("T100", "Volvo", "Diesel", "10-DF-E22");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        Bedrijf b = (Bedrijf) sce.getServletContext().getAttribute("Bedrijf");
        try {
            FileOutputStream fos = new FileOutputStream("/home/berend/bedrijf.obj");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(b);
            oos.close();
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
}
