/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.b3r3nd.reference;

import java.io.Serializable;

import com.b3r3nd.domein.agenda.Agenda;
import com.b3r3nd.domein.facturatie.Deurwaarder;
import com.b3r3nd.domein.garage.Garage;
import com.b3r3nd.domein.klantenbinding.Klandizie;
import com.b3r3nd.domein.voorraad.Magazijn;
import com.b3r3nd.user.UserControl;

/**
 * @author Rory
 */
public class Bedrijf implements Serializable {
    private final Agenda werkplaats;
    private final Garage parkeergarage; 
    private final Deurwaarder facturatie;
    private final Klandizie klantenbinding;
    private final Magazijn voorraadbeheer;
    private final UserControl userControl;
    
    public Bedrijf(){
        werkplaats = new Agenda();
        parkeergarage = new Garage();
        facturatie = new Deurwaarder();
        klantenbinding = new Klandizie();
        voorraadbeheer = new Magazijn();
        userControl = new UserControl(werkplaats, klantenbinding);
    }

    public Bedrijf(Agenda werkplaats, Garage parkeergarage, Deurwaarder facturatie, Klandizie klantenbinding, Magazijn voorraadbeheer, UserControl userControl) {
        this.werkplaats = werkplaats;
        this.parkeergarage = parkeergarage;
        this.facturatie = facturatie;
        this.klantenbinding = klantenbinding;
        this.voorraadbeheer = voorraadbeheer;
        this.userControl = userControl;
    }
    
    public UserControl getUserControl() {
        return userControl;
    }

    public Agenda getWerkplaats() {
        return werkplaats;
    }

    public Garage getParkeergarage() {
        return parkeergarage;
    }

    public Deurwaarder getFacturatie() {
        return facturatie;
    }

    public Klandizie getKlantenbinding() {
        return klantenbinding;
    }

    public Magazijn getVoorraadbeheer() {
        return voorraadbeheer;
    }
}
