/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.b3r3nd.domein.garage;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Rory
 */
public class Plek implements Serializable {
    private int pleknummer;
    private boolean bezet = false, schoon = false;
    private double prijs;
    
    private ArrayList<Reservering> deReserveringen = new ArrayList<>();
    private Auto deAuto;

    public Plek(int pleknummer, double prijs) {
        this.pleknummer = pleknummer;
        this.prijs = prijs;
    }

    public int getPleknummer() {
        return pleknummer;
    }

    public void setPleknummer(int pleknummer) {
        this.pleknummer = pleknummer;
    }

    public boolean isBezet() {
        return bezet;
    }

    public void setBezet(boolean bezet) {
        this.bezet = bezet;
    }

    public boolean isSchoon() {
        return schoon;
    }

    public void setSchoon(boolean schoon) {
        this.schoon = schoon;
    }

    public double getPrijs() {
        return prijs;
    }

    public void setPrijs(double prijs) {
        this.prijs = prijs;
    }

    public Auto getDeAuto() {
        return deAuto;
    }

    public void setDeAuto(Auto deAuto) {
        this.deAuto = deAuto;
    }
}
