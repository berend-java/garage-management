/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.b3r3nd.domein.garage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.b3r3nd.domein.voorraad.Onderdeel;

/**
 * @author Rory
 */
public class Garage implements Serializable {

    private ArrayList<Auto> deAutos = new ArrayList<>();
    private ArrayList<Plek> dePlekken = new ArrayList<>();
    private ArrayList<Reservering> deReserveringen = new ArrayList<>();

    public Garage() {
        for (int i = 1; i < 21; i++) {
            dePlekken.add(new Plek(i, 5.00));
        }
    }
    
    public ArrayList<Auto> getAutos() {
    	return deAutos;
    }

    public ArrayList<Auto> getAutosZonderEigenaar() {
        ArrayList<Auto> al = new ArrayList<>();

        for (Auto value : deAutos) {
            if (value.getDeEigenaar() == null) {
                al.add(value);
            }
        }

        return al;
    }
    
    public void removeAuto(Auto a) {
    	deAutos.remove(a);
    }

    public void addAuto2(Auto a) {
    	deAutos.add(a);
    }
    public boolean addAuto(String t, String m, String b, String k) {
        for (Auto value : deAutos) {
            if (value.getKenteken().equals(k)) {
                System.out.println("Kenteken bestaat al!");
                return false;
            }
        }
        deAutos.add(new Auto(t, m, b, k));
        return true;
    }
    
    public Auto zoekAuto(String kenteken) {
    	Auto a = null;
    	for(Auto a1: deAutos) {
    		if(a1.getKenteken().equals(kenteken)) {
    			a = a1;
    		}
    	}
    	
    	return a;
    }
    
    public Plek zoekPlek(int iPleknummer) {
    	Plek p = null;
    	for(Plek pp: dePlekken) {
    		if(pp.getPleknummer() == iPleknummer) {
    			p = pp;
    		}
    	}
    	return p;
    }

    public boolean plekReserveren(Auto a, Plek p, Date d) {
        if (p.isBezet()) {
            return false;
        }
        p.setBezet(true);
        deReserveringen.add(new Reservering(d, p, a));
        return true;
    }
    
    public ArrayList<Plek> getPlekken() {
    	return dePlekken;
    }
}
