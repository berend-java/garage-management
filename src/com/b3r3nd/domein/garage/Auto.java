/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.b3r3nd.domein.garage;

import java.io.Serializable;
import java.util.Date;
import com.b3r3nd.domein.klantenbinding.Klant;

/**
 * @author Rory
 */
public class Auto implements Serializable {
    private String type, merk, brandstof, kenteken;
    private Date laastOnderhoud;
    
    private Klant deEigenaar;

    public Auto(String type, String merk, String brandstof, String kenteken) {
        this.type = type;
        this.merk = merk;
        this.brandstof = brandstof;
        this.kenteken = kenteken;
        this.laastOnderhoud = new Date();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getBrandstof() {
        return brandstof;
    }

    public void setBrandstof(String brandstof) {
        this.brandstof = brandstof;
    }

    public String getKenteken() {
        return kenteken;
    }

    public void setKenteken(String kenteken) {
        this.kenteken = kenteken;
    }

    public Klant getDeEigenaar() {
        return deEigenaar;
    }

    public void setDeEigenaar(Klant deEigenaar) {
        this.deEigenaar = deEigenaar;
    }

    public Date getLaastOnderhoud() {
        return laastOnderhoud;
    }

    public void setLaastOnderhoud(Date laastOnderhoud) {
        this.laastOnderhoud = laastOnderhoud;
    }
    
    public String toString() {
    	return type + " " + merk + " " + kenteken;
    }
    
}
