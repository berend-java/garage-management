/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.b3r3nd.domein.garage;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Rory
 */
public class Reservering implements Serializable {
    private Date startdatum, einddatum;
    private boolean gereed = false;
    private Plek dePlek;
    private Auto deAuto;

    public Reservering(Date startdatum, Plek plek, Auto a) {
        this.startdatum = startdatum;
        this.dePlek = plek;
    }
    
//    public double getPrijs(){
//        int factor = startdatum - einddatum;
//        
//        return factor * dePlek.getPrijs();
//    }
    
    public boolean finish(){
        if(gereed)
            return false;
        dePlek.setBezet(false);
        einddatum = new Date();
        gereed = true;
        return true;
    }

    public Date getStartdatum() {
        return startdatum;
    }

    public void setStartdatum(Date startdatum) {
        this.startdatum = startdatum;
    }

    public Date getEinddatum() {
        return einddatum;
    }

    public void setEinddatum(Date einddatum) {
        this.einddatum = einddatum;
    }

    public boolean isGereed() {
        return gereed;
    }

    public void setGereed(boolean gereed) {
        this.gereed = gereed;
    }
    
    public Plek getPlek() {
        return dePlek;
    }
}
