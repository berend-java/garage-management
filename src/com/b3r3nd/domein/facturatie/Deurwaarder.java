/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.b3r3nd.domein.facturatie;

import java.io.Serializable;
import java.util.ArrayList;

import com.b3r3nd.user.Gebruiker;

/**
 * @author Rory
 */
public class Deurwaarder implements Serializable{
    private ArrayList<Factuur> deParkFact = new ArrayList<>();
    private ArrayList<Factuur> deRepFact = new ArrayList<>();
    private ArrayList<Factuur> deTankFact = new ArrayList<>();
    
    public ArrayList<Factuur> getFacturen() {
        ArrayList<Factuur> facturen = new ArrayList<>();

        facturen.addAll(deParkFact);
        facturen.addAll(deRepFact);
        facturen.addAll(deTankFact);

        return facturen;
    }
}
