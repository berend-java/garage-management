/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.b3r3nd.domein.facturatie;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Rory
 */
public abstract class Factuur {
    private Date datum;
    
    private boolean betaald = false, gereed = false;
	public boolean isBetaald() {
		return betaald;
	}
	public void setBetaald(boolean betaald) {
		this.betaald = betaald;
	}
	public Date getDatum() {
		return datum;
	}
	public void setDatum(Date datum) {
		this.datum = datum;
	}
	public boolean isGereed() {
		return gereed;
	}
	public void setGereed(boolean gereed) {
		this.gereed = gereed;
	}
}
