/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.b3r3nd.domein.agenda;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.b3r3nd.domein.garage.Auto;

/**
 * @author Rory
 */
public class Agenda implements Serializable {
    private final ArrayList<Monteur> deMonteurs = new ArrayList<>();
    private final ArrayList<Reparatie> deReparaties = new ArrayList<>();
    private final ArrayList<Werkzaamheid> werkzaamheden = new ArrayList<>();

    
    public Agenda(){
    }

    public ArrayList<Monteur> getDeMonteurs() {
        return deMonteurs;
    }

    public ArrayList<Reparatie> getDeReparaties() {
        return deReparaties;
    }
    
    public void addWerkzaamheid(Werkzaamheid h) {
    	werkzaamheden.add(h);
    }
    
    public void removeWerkzaamheid(Werkzaamheid h) {
    	werkzaamheden.remove(h);
    }
    
    public void removeMonteur(Monteur h) {
    	deMonteurs.remove(h);
    }
    
    public Werkzaamheid zoekWerkzaamheid(String omschrijving) {
    	Werkzaamheid h = null;
    	for(Werkzaamheid w: werkzaamheden) {
    		if(w.getOmschrijving().equals(omschrijving)) {
    			h = w;
    		}
    	}
    	return h;
    }
    
    public boolean koppelMonRep(Monteur m, Reparatie r){
        if(!checkReparatie(r) && !checkMonteur(m)){
            r.addMonteur(m);
            return true;
        }
        return false;
    }
    
    public boolean addReparatie(String status, String naam, Date begindatum, Auto auto){
        Reparatie r = new Reparatie(status, naam, begindatum, auto);
        if(checkReparatie(r)){
            deReparaties.add(r);
            return true;
        }
        return false;
    }
    
    public void removeReparatie(Reparatie r) {
    	deReparaties.remove(r);
    }
    
    public boolean addMonteur(String voornaam, String achternaam, Date geboortedatum){
        Monteur m = new Monteur(voornaam, achternaam, geboortedatum);
        if(checkMonteur(m)){
            deMonteurs.add(m);
            return true;
        }
        return false;
    }
    
    public boolean addMonteur(Monteur m){
        if(checkMonteur(m)){
            deMonteurs.add(m);
            return true;
        }
        return false;
    }
    
    private boolean checkReparatie(Reparatie r){
        for(Reparatie value : deReparaties){
            if(value==r)
                return false;
        }
        return true;
    }
    private boolean checkMonteur(Monteur m){
        for(Monteur value : deMonteurs){
            if(value==m)
                return false;
        }
        return true;
    }
    
    public Reparatie zoekReparatie(String nm) {
    	Reparatie repa = null;
    	for(Reparatie r: deReparaties) {
    		if(r.getNaam().equals(nm)) {
    			repa = r;
    		}
    	}
    	return repa;
    }
    
    public Monteur zoekMonteur(String nm) {
    	Monteur m = null;
    	for(Monteur mm: deMonteurs) {
    		if(mm.getUsername().equals(nm)) {
    			m = mm;
    		}
    	}
    	return m;
    }

	public ArrayList<Werkzaamheid> getWerkzaamheden() {
		return werkzaamheden;
	}
}
