/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.b3r3nd.domein.agenda;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import com.b3r3nd.domein.garage.Auto;
import com.b3r3nd.domein.voorraad.Brandstof;
import com.b3r3nd.domein.voorraad.Onderdeel;

/**
 * @author Rory
 */
public class Reparatie implements Serializable {
    private String status, naam;
    private Date begindatum, einddatum;
    private boolean gereed = false;
    
    private ArrayList<Werkzaamheid> deWerkzaamheden = new ArrayList<>();
    private ArrayList<Monteur> deMonteurs = new ArrayList<>();
    private Auto deAuto;
    private ArrayList<Brandstof> deBrandstoffen = new ArrayList<>();
    private ArrayList<Onderdeel> deOnderdelen = new ArrayList<>();

    public Reparatie(String status, String naam, Date begindatum, Auto auto) {
        this.status = status;
        this.naam = naam;
        this.begindatum = begindatum;
        deAuto = auto;
    }
    
//    public int getManuur(){
//        return deMonteurs.size() * (einddatum - begindatum) * 8;
//    }
    
    public boolean finish(){
        if(gereed)
            return false;
        gereed = true;
        einddatum = new Date();
        status = "klaar";
        for(Monteur m : deMonteurs)
            m.setBezet(false);
        return true;
    }
    
    public Auto getDeAuto() {
    	return deAuto;
    }
    public boolean addOnderdeel(Onderdeel o){
        if(checkOnderdeel(o)){
            deOnderdelen.add(o);
            return true;
        }
        return false;
    }
    public boolean addBrandstof(Brandstof b){
        if(checkBrandstof(b)){
            deBrandstoffen.add(b);
            return true;
        }
        return false;
    }
    
    private boolean checkBrandstof(Brandstof b){
        for(Brandstof value : deBrandstoffen){
            if(value==b){
                return false;
            }
        }
        return true;
    }
    private boolean checkOnderdeel(Onderdeel o){
        for(Onderdeel value : deOnderdelen){
            if(value==o){
                return false;
            }
        }
        return true;
    }
    
    public boolean koppelMonWerk(Monteur m, Werkzaamheid w){
        if(!checkMonteur(m) && !checkWerkzaamheid(w)){
            m.addWerkzaamheid(w);
            return true;
        }
        return false;
    }

    private boolean checkMonteur(Monteur m){
        for(Monteur value : deMonteurs){
            if(value==m)
                return false;
        }
        return true;
    }
    public boolean addMonteur(Monteur m){
        if(checkMonteur(m) && !m.isBezet()){
            deMonteurs.add(m);
            return true;
        }
        return false;
    }
    
    public void addWerkzaamheid(String omschrijving, Date begindatum){
        Werkzaamheid w = new Werkzaamheid(omschrijving, begindatum);
    }
    public void addWerkzaamheid2(Werkzaamheid w) {
    	deWerkzaamheden.add(w);
    }
    private boolean checkWerkzaamheid(Werkzaamheid w){
        for(Werkzaamheid value : deWerkzaamheden){
            if(value==w)
                return false;
        }
        return true;
    }
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getBegindatum() {
        return begindatum;
    }

    public void setBegindatum(Date begindatum) {
        this.begindatum = begindatum;
    }

    public Date getEinddatum() {
        return einddatum;
    }

    public void setEinddatum(Date einddatum) {
        this.einddatum = einddatum;
    }

    public ArrayList<Werkzaamheid> getDeWerkzaamheden() {
        return deWerkzaamheden;
    }

    public ArrayList<Monteur> getDeMonteurs() {
        return deMonteurs;
    }
    
    public ArrayList<Onderdeel> getDeOnderdelen() {
    	return deOnderdelen;
    }
    
    public String getNaam(){
    	return naam;
    }
    
    public boolean isGereed() {
    	return gereed;
    }
    
    public void setIsGereed(boolean b) {
    	gereed = b;
    }
}
