/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.b3r3nd.domein.klantenbinding;

import java.io.Serializable;
import java.util.Date;
import com.b3r3nd.domein.garage.Auto;

/**
 * @author Rory
 */
public class AutoOnderhoud extends Herinnering implements Serializable {
    private Auto deAuto;

    public AutoOnderhoud(Auto deAuto) {
        this.deAuto = deAuto;
        datum = new Date();
        
        bericht = "Beste heer/mevrouw " + deAuto.getDeEigenaar().getAchternaam() + ", \n Uw auto " + deAuto.getMerk() + " heeft onderhoud nodig.";
    }

    public Auto getDeAuto() {
        return deAuto;
    }
}
