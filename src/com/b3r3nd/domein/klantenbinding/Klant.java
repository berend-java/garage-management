/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.b3r3nd.domein.klantenbinding;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.b3r3nd.domein.garage.Auto;
import com.b3r3nd.user.Gebruiker;

/**
 * @author Rory
 */
public class Klant extends Gebruiker implements Serializable {

    private String voornaam, achternaam, email;
    private Date geboortedatum, laatstGeweest;
    private ArrayList<Auto> deAutos = new ArrayList<>();

    public Klant(String voornaam, String achternaam, String email, Date geboortedatum) {
        this.voornaam = voornaam;
        this.achternaam = achternaam;
        this.email = email;
        this.geboortedatum = geboortedatum;
        laatstGeweest = new Date();
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public String getAchternaam() {
        return achternaam;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getGeboortedatum() {
        return geboortedatum;
    }

    public String getGeboorteDatumString() {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Date datum = geboortedatum;
        String sDatum = df.format(datum);
        return sDatum;
    }

    public void setGeboortedatum(Date geboortedatum) {
        this.geboortedatum = geboortedatum;
    }

    public ArrayList<Auto> getDeAutos() {
        return deAutos;
    }

    public Date getLaatstGeweest() {
        return laatstGeweest;
    }

    public void setLaatstGeweest(Date laatstGeweest) {
        this.laatstGeweest = laatstGeweest;
    }

    public int getAantalAutos() {
        return deAutos.size();
    }

    public String toString() {
        return voornaam + " " + achternaam;
    }

    public boolean addAuto(Auto a) {
        if (a.getDeEigenaar() != null) 
            return false;
        for (Auto value : deAutos) {
            if (value == a) 
                return false;
        }
        deAutos.add(a);
        return true;
    }
}
