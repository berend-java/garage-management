/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.b3r3nd.domein.klantenbinding;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Rory
 */
public class LangNietGeweest extends Herinnering implements Serializable {
    private Klant deKlant;
    
    public LangNietGeweest(Klant k) {
        deKlant = k;
        datum = new Date();
        
        bericht = "Beste heer/mevrouw " + k.getAchternaam();
    }

    public Klant getDeKlant() {
        return deKlant;
    }

    public void setDeKlant(Klant deKlant) {
        this.deKlant = deKlant;
    }
}
