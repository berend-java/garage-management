/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.b3r3nd.domein.klantenbinding;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import com.b3r3nd.domein.garage.Auto;

/**
 * @author Rory
 */
public class Klandizie implements Serializable {
    private ArrayList<Klant> deKlanten = new ArrayList<>();
    private ArrayList<Herinnering> deKlantMeldingen = new ArrayList<>();
    private ArrayList<Herinnering> deAutoMeldingen = new ArrayList<>();

    public Klandizie() {
    }
    
    public ArrayList<Auto> genereerAOMelding(){
        ArrayList<Auto> al = new ArrayList<>();
        
        Date d = new Date();
        d.setMonth(d.getMonth()-6);
        
        for(Klant value : deKlanten){
            for(Auto value2 : value.getDeAutos()){
                if(value2.getLaastOnderhoud().before(d))
                    al.add(value2);
            }
        }
        
        return al;
    }
    public void saveAOMelding(ArrayList<Auto> al){
        for(Auto value : al)
            addAutoMelding(value);
    }
    
    public void removeKlant(Klant k) {
    	deKlanten.remove(k);
    }
    
    public ArrayList<Klant> genereerLNGMelding(){
        ArrayList<Klant> al = new ArrayList<>();
        
        Date d = new Date();
        d.setMonth(d.getMonth()-2);
        
        for(Klant value : deKlanten){
            if(value.getLaatstGeweest().before(d))
                al.add(value);
        }
        
        return al;
    }
    public void saveLNGMelding(ArrayList<Klant> al){
        for(Klant value : al){
            addKlantMelding(value);
        }
    }
    
    public boolean koppelAutoKlant(Auto a, Klant k){
        if(a.getDeEigenaar()!=null)
            return false;
        k.addAuto(a);
        a.setDeEigenaar(k);
        return true;
    }
    
    public void addKlant(String voornaam, String achternaam, String email, Date geboortedatum){
        Klant k = new Klant(voornaam, achternaam, email, geboortedatum);
        deKlanten.add(k);
    }
    public void addKlant(Klant k){
        deKlanten.add(k);
    }
    private void addKlantMelding(Klant k){
        Herinnering h = new LangNietGeweest(k);
        deKlantMeldingen.add(h);
    }
    private void addAutoMelding(Auto a){
        Herinnering h = new AutoOnderhoud(a);
        deAutoMeldingen.add(h);
    }

    public ArrayList<Klant> getDeKlanten() {
        return deKlanten;
    }

    public ArrayList<Herinnering> getDeKlantMeldingen() {
        return deKlantMeldingen;
    }

    public ArrayList<Herinnering> getDeAutoMeldingen() {
        return deAutoMeldingen;
    }
    
    public Klant zoekKlant(String username) {
    	Klant k1 = null;
    	for(Klant k: deKlanten) {
    		if(k.getUsername().equals(username)) {
    			k1 = k;
    		}
    	}
    	return k1;
    }
}
