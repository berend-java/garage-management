package com.b3r3nd.domein.klantenbinding;

import java.util.Date;

/**
 * @author Rory
 */
public abstract class Herinnering {
    protected Date datum;
    protected String bericht;

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public String getBericht() {
        return bericht;
    }

    public void setBericht(String bericht) {
        this.bericht = bericht;
    }
}
