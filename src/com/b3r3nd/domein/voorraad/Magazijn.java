/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.b3r3nd.domein.voorraad;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Rory
 */
public class Magazijn implements Serializable {
    private ArrayList<Brandstof> deBrandstoffen = new ArrayList<>();
    private ArrayList<Onderdeel> deOnderdelen = new ArrayList<>();

	public Magazijn(){
        deBrandstoffen.add(new Brandstof("Diesel", 10.0, 1.50, 1));
        deBrandstoffen.add(new Brandstof("Benzine", 10.0, 1.67, 2));
    }
    
    //maakt een nieuw onderdeel object aan als deze nog niet bestaat, anders verhoogt die het aantal van het bestaande onderdeel
    public boolean addOnderdeel(int artikelnummer, int aantal, double prijs, String type){
        Onderdeel o = new Onderdeel();
        for(Onderdeel value : deOnderdelen){
            if(value.getArtikelnummer() == artikelnummer){
                o = value;
            }
        }
        if(o.getArtikelnummer() == artikelnummer){
            if(o.getType().equals(type)){
                o.setAantal(o.getAantal()+aantal);
                return true;
            }else
                return false;
        }else{
            deOnderdelen.add(new Onderdeel(artikelnummer, aantal, prijs, type));
            return true;
        }
    }
    
    public boolean addBrandstof(String type, double al){
        switch (type) {
            case "Diesel":
                for(Brandstof value : deBrandstoffen){
                    if(value.getType().equals("Diesel")){
                        value.setAantalLiters(value.getAantalLiters()+al);
                        return true;
                    }
                }
            case "Benzine":
                for(Brandstof value : deBrandstoffen){
                    if(value.getType().equals("Benzine")){
                        value.setAantalLiters(value.getAantalLiters()+al);
                    }
                }
            default:
                return false;
        }
    }
    //maakt een nieuw object aan van mee gegeven artikelnummer met meegegeven aantal
    public Onderdeel getOnderdeel(int an, int a){
        Onderdeel o = new Onderdeel();
        for(Onderdeel value : deOnderdelen){
            if(value.getArtikelnummer()==an)
                o = value;
        }
        o.setAantal(o.getAantal()-a);
        return new Onderdeel(an, a, o.getPrijs(), o.getType());
    }
    //kijkt of er genoeg aantal onderdelen van meegegeven artikelnummer zijn
    public boolean checkOnderdeel(int an, int a){
        Onderdeel o = new Onderdeel();
        for(Onderdeel value : deOnderdelen){
            if(value.getArtikelnummer()==an)
                o = value;
        }
        return o.getAantal()>a;
    }
    //maakt een nieuw object van gegeven type brandstof met gegeven aantal liters
    public Brandstof getBrandstof(int l, String t){
        Brandstof b = new Brandstof();
        switch (t) {
            case "Diesel":
                for(Brandstof value : deBrandstoffen){
                    if(value.getType().equals("Diesel"))
                        b = value;
                }   break;
            case "Benzine":
                for(Brandstof value : deBrandstoffen){
                    if(value.getType().equals("Benzine"))
                        b = value;
                }   break;
            default:
                return null;
        }
        b.setAantalLiters(b.getAantalLiters()-l);
        return new Brandstof(t, l, b.getPrijsPerLiter(), b.getTSIC());
    }
    //kijkt of er genoeg liters aanwezig zijn van meegegeven type
    public boolean checkBrandstof(int l, String t){
        Brandstof b = new Brandstof();
        if(!t.equals("Benzine") && !t.equals("Diesel"))
            return false;
        
        switch (t) {
            case "Diesel":
                for(Brandstof value : deBrandstoffen){
                    if(value.getType().equals("Diesel"))
                        b = value;
                }
            case "Benzine":
                for(Brandstof value : deBrandstoffen){
                    if(value.getType().equals("Benzine"))
                        b = value;
                }
        }
        return b.getAantalLiters()>l;
    }
    
    public void removeOnderdeel(Onderdeel o) {
    	deOnderdelen.remove(o);
    }
    
    public Onderdeel zoekOnderdeel(String type) {
    	Onderdeel tempo = null;
    	for(Onderdeel o: deOnderdelen) {
    		if(o.getType().equals(type)) {
    			tempo = o;
    		}
    	}
    	
    	return tempo;
    }
    
    public ArrayList<Brandstof> getDeBrandstoffen() {
		return deBrandstoffen;
	}

	public ArrayList<Onderdeel> getDeOnderdelen() {
		return deOnderdelen;
	}
}
