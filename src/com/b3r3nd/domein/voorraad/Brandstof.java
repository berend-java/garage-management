/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package  com.b3r3nd.domein.voorraad;

import java.io.Serializable;

/**
 * @author Rory
 */
public class Brandstof implements Serializable {
    private String type;
    private double aantalLiters, prijsPerLiter;
    private int TSIC;

    public Brandstof(String type, double aantalLiters, double prijsPerLiter, int TSIC) {
        this.type = type;
        this.aantalLiters = aantalLiters;
        this.prijsPerLiter = prijsPerLiter;
        this.TSIC = TSIC;
    }
    public Brandstof(){};

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAantalLiters() {
        return aantalLiters;
    }

    public void setAantalLiters(double aantalLiters) {
        this.aantalLiters = aantalLiters;
    }

    public double getPrijsPerLiter() {
        return prijsPerLiter;
    }

    public void setPrijsPerLiter(double prijsPerLiter) {
        this.prijsPerLiter = prijsPerLiter;
    }

    public int getTSIC() {
        return TSIC;
    }

    public void setTSIC(int TSIC) {
        this.TSIC = TSIC;
    }
    public String toString() {
    	return type;
    }
}
