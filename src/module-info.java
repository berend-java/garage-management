/**
 * 
 */
/**
 * @author beren
 *
 */
module Project {
	exports com.b3r3nd.login;
	exports com.b3r3nd.domein.agenda;
	exports com.b3r3nd.domein.facturatie;
	exports com.b3r3nd.domein.voorraad;
	exports com.b3r3nd.servlets;
	exports com.b3r3nd.domein.klantenbinding;
	exports com.b3r3nd.user;
	exports com.b3r3nd.listeners;
	exports com.b3r3nd.domein.garage;
	exports com.b3r3nd.reference;
}