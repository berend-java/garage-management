<%@include file = "/layout.jsp" %>
<%@ page import="com.b3r3nd.domein.agenda.Agenda" %>
<%@ page import="com.b3r3nd.domein.garage.Plek" %>\
<%@ page import="com.b3r3nd.reference.Bedrijf" %>
<%@ page import="java.util.ArrayList" %>
<div class='content'>

<div class='title'>
    <h1>Voorraad</h1>
</div>
<div class='container'>
	<table border='0'> 
		<tr class='headtable'>
			<td width='120'><b>Pleknummer</b></td>
			<td width='130'><b>bezet</b></td>
			<td width='130'><b>Schoon</b></td>
			<td width='130'><b>Prijs</b></td>
			<td width='140'><b>Actions</b></td>
            </tr>
	</table>
<div id='tickets'>
<table border='0'>
<% Bedrijf b = (Bedrijf)getServletContext().getAttribute("Bedrijf");
	ArrayList<Plek> plekken = b.getParkeergarage().getPlekken();
	int ii = 0;

for (Plek p: plekken) {
	if(ii == 0) { ii++; }
	else if(ii == 1) { ii--; }
	out.println(" <tr class='table"+ii+"'>" + 
            "<td width='120'>"+p.getPleknummer()+"</td>" +
            "<td width='130'>"+p.isBezet()+"</td>" +
            "<td width='130'>"+p.isSchoon()+"</td>" +
            "<td width='130'>"+p.getPrijs()+"</td>" +
            "<td width='140'><a href='/Project/secure/garage/reserveren.jsp?plek="+p.getPleknummer()+"'><img title='Plek Reserveren' src='/Project/images/addonderdelen.png' height='20'></a><a href='/Project/secure/garage/vrijmaken.jsp?plek="+p.getPleknummer()+"'><img title='Plek Vrijmaken' src='/Project/images/delete.png' height='20'></a></td></tr>");
}
%>
</table>
	<ul>
		<li><img title='Plek Reserveren' src='/Project/images/addonderdelen.png' height='20' />- Plek Reserveren</a><br></li>
		<li><img title='Plek Vrijmaken' src='/Project/images/delete.png' height='20' />- Plek Vrijmaken</a><br></li>
	</ul>
<p class='padding'>
</p>
</div>
</div>
</div>
<%@include file = "/footer.jsp" %>