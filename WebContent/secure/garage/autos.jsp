<%@include file = "/layout.jsp" %>
<%@ page import="com.b3r3nd.domein.agenda.Agenda" %>
<%@ page import="com.b3r3nd.domein.garage.Auto" %>
<%@ page import="com.b3r3nd.reference.Bedrijf" %>
<%@ page import="java.util.ArrayList" %>
<div class='content'>

<div class='title'>
    <h1>Autos</h1>
</div>
<div class='container'>
	<table border='0'> 
		<tr class='headtable'>
			<td width='120'><b>Kenteken</b></td>
			<td width='110'><b>Type</b></td>
			<td width='130'><b>Merk</b></td>
			<td width='120'><b>Brandstof</b></td>
			<td width='100'><b>Eigenaar</b></td>
			<td witdth="140"><b>Actions</b></td>
            </tr>
	</table>
<div id='tickets'>
<table border='0'>
<% Bedrijf b = (Bedrijf)getServletContext().getAttribute("Bedrijf");
	ArrayList<Auto> autos = b.getParkeergarage().getAutos();
	int i = 0;

for (Auto a: autos) {
	if(i == 0) { i++; }
	else if(i == 1) { i--; }
	out.println(" <tr class='table"+i+"'>" + 
            "<td width='110'>"+a.getKenteken()+"</td>" +
            "<td width='110'>"+a.getType()+"</td>" +
            "<td width='130'>"+a.getMerk()+"</td>" +
            "<td width='130'>"+a.getBrandstof()+"</td>" +
            "<td width='100'>"+a.getDeEigenaar().getUsername()+"</td>" +
            "<td witdth='140'><a href='/Project/secure/garage/auto.jsp?auto="+a.getKenteken()+"'><img title='Edit Auto' src='/Project/images/edit.png' height='20'></a><a href='/Project/delauto?auto="+a.getKenteken()+"'><img title='Delete Auto' src='/Project/images/delete.png' height='20'></a></td></tr>");
}
%>
</table>
	<ul>
		<li><a href='/Project/secure/garage/newauto.jsp'><img title='Nieuwe Auto' src='/Project/images/auto.png' height='20' />- Nieuwe Auto</a><br></li>
		<li><img title='Edit Auto' src='/Project/images/edit.png' height='20' />- Edit Auto<br></li>
		<li><img title='Delete Auto' src='/Project/images/delete.png' height='20' />- Delete Auto<br></li>
	</ul>
<p class='padding'>
</p>
</div>
</div>
</div>
<%@include file = "/footer.jsp" %>