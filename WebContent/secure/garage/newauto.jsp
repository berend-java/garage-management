<%@include file = "/layout.jsp" %>
<%@ page import="com.b3r3nd.reference.Bedrijf" %>
<%@ page import="com.b3r3nd.domein.klantenbinding.Klant" %>
<%@ page import="java.util.ArrayList" %>

<%
Bedrijf b = (Bedrijf)getServletContext().getAttribute("Bedrijf");
ArrayList<Klant> klanten = b.getKlantenbinding().getDeKlanten();
%> 
<div class='title'>
    <h2>Nieuwe Auto aanmaken</h2>
</div>
<div class='banner'>
    <table border='0'>
        <tr>
            <td width='380'></td>
            </tr>
    </table>
</div>
<div class='container'>
    <div class='tickets'>
		<form action='/Project/newauto' >
		<table border='0'>
			<tr><td width='150'>Kenteken: </td><td width='500'><input type='text' name='kenteken' value='${param.kenteken}'/></td></tr>
			<tr><td>Type: </td><td><input type='text' name='type' value='${param.type}'/> </td></tr>
			<tr><td width='150'>Merk: </td><td width='500'><input type='text' name='merk'value='${param.merk}' /></td></tr>
			<tr><td>Brandstof: </td><td><input type='text' name='brandstof' value='${param.brandstof}' /> </td></tr>
			<tr><td>Klant</td><td><select name='klant'>
			<%
			for(Klant k: klanten) {
				out.println("<option value='"+k.getUsername()+"'>" +k.getUsername()+"</option>");
			}
			%>
			</select></td></tr>
			<tr><td collspan='2'><input title='Aanmaken' type='image' src='/Project/images/login.png' name='submit' value='Aanmaken' /></td></tr>
		</table></form>
		<br><% if(request.getAttribute("fout") != null) {
				out.println(request.getAttribute("fout"));
			}
			%>
	</div>
</div>