<%@include file = "/layout.jsp" %>
<%@ page import="com.b3r3nd.reference.Bedrijf" %>
<%@ page import="com.b3r3nd.domein.klantenbinding.Klant" %>
<%@ page import="com.b3r3nd.domein.garage.Auto" %>
<%@ page import="java.util.ArrayList" %>

<%
Bedrijf b = (Bedrijf)getServletContext().getAttribute("Bedrijf");
ArrayList<Klant> klanten = b.getKlantenbinding().getDeKlanten();

Auto a = b.getParkeergarage().zoekAuto(request.getParameter("auto"));
%> 
<div class='title'>
    <h2>Edit Auto </h2>
</div>
<div class='banner'>
    <table border='0'>
        <tr>
            <td width='380'></td>
            </tr>
    </table>
</div>
<div class='container'>
    <div class='tickets'>
		<form action='/Project/editauto' >
		<table border='0'>
			<tr><td width='150'>Kenteken: </td><td width='500'><input type='text' name='kenteken' value='<%=a.getKenteken() %>'/></td></tr>
			<tr><td>Type: </td><td><input type='text' name='type' value='<%=a.getType() %>'/> </td></tr>
			<tr><td width='150'>Merk: </td><td width='500'><input type='text' name='merk'value='<%=a.getMerk() %>' /></td></tr>
			<tr><td>Brandstof: </td><td><input type='text' name='brandstof' value='<%=a.getBrandstof() %>' /> </td></tr>
			<tr><td collspan='2'><input title='Edit' type='image' src='/Project/images/login.png' name='submit' value='Edit' /></td></tr>
		</table>
		<input type='hidden' name='auto' value='<%=request.getParameter("auto") %>' /></form>
		<br><% if(request.getAttribute("fout") != null) {
				out.println(request.getAttribute("fout"));
			}
			%>
	</div>
</div>