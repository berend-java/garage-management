<%@include file = "/layout.jsp" %>
<%@ page import="com.b3r3nd.reference.Bedrijf" %>
<%@ page import="com.b3r3nd.domein.klantenbinding.Klant" %>
<%@ page import="com.b3r3nd.domein.garage.Auto" %>
<%@ page import="java.util.ArrayList" %>

<%
Bedrijf b = (Bedrijf)getServletContext().getAttribute("Bedrijf");
ArrayList<Klant> klanten = b.getKlantenbinding().getDeKlanten();
ArrayList<Auto> autos = b.getParkeergarage().getAutos();

%> 
<div class='title'>
    <h2>Reserveer Plek</h2>
</div>
<div class='banner'>
    <table border='0'>
        <tr>
            <td width='380'></td>
            </tr>
    </table>
</div>
<div class='container'>
    <div class='tickets'>
		<form action='/Project/reserveer' >
		<table border='0'>
			<tr><td width='150'>Datum: </td><td width='500'><input type='text' name='datum' value='${param.datum}'/></td></tr>
			<tr><td>Auto</td><td><select name='auto'>
			<%
			for(Auto a: autos) {
				out.println("<option value='"+a.getKenteken()+"'>"+a.getType()+" " + a.getMerk()+"</option>");
			}
			%>
			</select></td></tr>
			<tr><td collspan='2'><input title='Aanmaken' type='image' src='/Project/images/login.png' name='submit' value='Aanmaken' /></td></tr>
			<input type="hidden" name="plek" value="<%=request.getParameter("plek") %>" />
		</table></form>
		<br><% if(request.getAttribute("fout") != null) {
				out.println(request.getAttribute("fout"));
			}
			%>
	</div>
</div>