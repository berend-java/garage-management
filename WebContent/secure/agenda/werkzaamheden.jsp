<%@include file = "/layout.jsp" %>
<%@ page import="com.b3r3nd.domein.agenda.Agenda" %>
<%@ page import="com.b3r3nd.domein.agenda.Werkzaamheid" %>
<%@ page import="com.b3r3nd.reference.Bedrijf" %>
<%@ page import="java.util.ArrayList" %>
<div class='content'>

<div class='title'>
    <h1>Werkzaamheden</h1>
</div>
<div class='container'>
	<table border='0'> 
		<tr class='headtable'>
			<td width='300'><b>Omschrijving</b></td>
			<td width='300'><b>Status</b></td>
			<td width='80'><b>Actions</b></td>
            </tr>
	</table>
<div id='tickets'>
<table border='0'>
<% Bedrijf b = (Bedrijf)getServletContext().getAttribute("Bedrijf");
	ArrayList<Werkzaamheid> werkzaamheden = b.getWerkplaats().getWerkzaamheden();
	int i = 0;

for (Werkzaamheid w: werkzaamheden) {
	if(i == 0) { i++; }
	else if(i == 1) { i--; }
	String bb = "Vrij";
	if(!w.isGereed()) {
		bb = "Bezet";
	}
	out.println(" <tr class='table"+i+"'>" + 
            "<td width='300'>"+w.getOmschrijving()+"</td>" +
            "<td width='300'>"+ bb+"</td>" +
            "<td width='80'><a href='/Project/delwerkzaamheid?werkzaamheid="+w.getOmschrijving()+"'><img title='Delete Werkzaamheid' src='/Project/images/delete.png' height='20'></a></td></tr>");
}
%>
</table>
	<ul>
		<li><a href='/Project/secure/agenda/newwerkzaamheid.jsp'><img title='Nieuwe Werkzaamheid' src='/Project/images/werkzaamheden.png' height='20' />- Nieuwe Werkzaamheid</a><br></li>

	</ul>
<p class='padding'>
</p>
</div>
</div>
</div>
<%@include file = "/footer.jsp" %>