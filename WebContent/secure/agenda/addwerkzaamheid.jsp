<%@include file = "/layout.jsp" %>
<%@ page import="com.b3r3nd.reference.Bedrijf" %>
<%@ page import="com.b3r3nd.domein.agenda.Werkzaamheid" %>
<%@ page import="java.util.ArrayList" %>

<%
Bedrijf b = (Bedrijf)getServletContext().getAttribute("Bedrijf");
ArrayList<Werkzaamheid> werkzaamheden = b.getWerkplaats().getWerkzaamheden();
%>
<div class='title'>
    <h2>Werkzaamheid Toevoegen Reparatie</h2>
</div>
<div class='banner'>
    <table border='0'>
        <tr>
            <td width='380'></td>
            </tr>
    </table>
</div>
<div class='container'>
    <div class='tickets'>
		<form action='/Project/koppelwerkzaamheid' ><input type='hidden' name='reparatie' value='<%=request.getParameter("reparatie") %>' />
		<table border='0'>
			<tr><td>Einddatum: </td><td><input type='text' name='einddatum' value='${param.einddatum}'/> </td></tr>
			<tr><td>Werkzaamheid</td><td><select name='werkzaamheid'>
			<%
			for(Werkzaamheid w: werkzaamheden) {
				out.println("<option value='"+w.getOmschrijving()+"'>"+w.getOmschrijving()+"</option>");
			}
			%>
			</select></td></tr>
			<tr><td collspan='2'><input title='Aanmaken' type='image' src='/Project/images/login.png' name='submit' value='Inloggen' /></td></tr>
		</table></form>
		<br><% if(request.getAttribute("fout") != null) {
				out.println(request.getAttribute("fout"));
			}
			%>
	</div>
</div>