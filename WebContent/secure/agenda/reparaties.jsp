<%@include file = "/layout.jsp" %>
<%@ page import="com.b3r3nd.domein.agenda.Agenda" %>
<%@ page import="com.b3r3nd.domein.agenda.Reparatie" %>
<%@ page import="com.b3r3nd.reference.Bedrijf" %>
<%@ page import="java.util.ArrayList" %>
<div class='content'>

<div class='title'>
    <h1>Reparaties</h1>
</div>
<div class='container'>
	<table border='0'> 
		<tr class='headtable'>
			<td width='210'><b>Naam</b></td>
			<td width='210'><b>Status</b></td>
			<td width='210'><b>Gereed</b></td>
			<td with='200'><b>Actions</b></td>
            </tr>
	</table>
<div id='tickets'>
<table border='0'>
<% Bedrijf b = (Bedrijf)getServletContext().getAttribute("Bedrijf");
	ArrayList<Reparatie> reparaties = b.getWerkplaats().getDeReparaties();
	int i = 0;

for (Reparatie r: reparaties) {
	if(i == 0) { i++; }
	else if(i == 1) { i--; }
	out.println(" <tr class='table"+i+"'>" + 
            "<td width='200'>"+r.getNaam()+"</td>" +
            "<td width='200'>"+r.getStatus()+"</td>" +
            "<td width='140'>"+r.isGereed()+"</td>" +
            "<td witth='190'>"+
            "<a href='/Project/secure/agenda/addwerkzaamheid.jsp?reparatie="+r.getNaam()+"'><img title='Add Werkzaamheid' src='/Project/images/werkzaamheden.png' height='20'></a>"+
            "<a href='/Project/secure/agenda/addmonteur.jsp?reparatie="+r.getNaam()+"'><img title='Add Monteur' src='/Project/images/monteurs.png' height='20'></a>"+
            "<a href='/Project/secure/agenda/reparatie.jsp?reparatie="+r.getNaam()+"'><img title='Edit Reparatie' src='/Project/images/edit.png' height='20'></a>"+
            "<a href='/Project/delrep?reparatie="+r.getNaam()+"'><img title='Delete Reparatie' src='/Project/images/delete.png' height='20'></a>"+
            "<a href='/Project/secure/agenda/addonderdelen.jsp?reparatie="+r.getNaam()+"'><img title='Goederen Toevoegen' src='/Project/images/addonderdelen.png' height='20'></a>"+
            "</td></tr>");
}
%>
</table>
	<ul>
		<li><a href='/Project/secure/agenda/newreparatie.jsp'><img title='Nieuwe Reparatie' src='/Project/images/newrep.png' height='20' />- Nieuwe Reparatie</a><br></li>
		<li><img title='Edit Reparatie' src='/Project/images/edit.png' height='20' />- Edit Reparatie<br></li>
		<li><img title='Delete Reparatie' src='/Project/images/delete.png' height='20' />- Delete Reparatie<br></li>
		<li><img title='Goederen Toevoegen' src='/Project/images/addonderdelen.png' height='20' />- Goederen toevoegen Reparatie<br></li>
		<li><img title='Add Monteur' src='/Project/images/monteurs.png' height='20' />- Monteur Toevoegen Reparatie<br></li>
		<li><img title='Add Werkzaamheid' src='/Project/images/werkzaamheden.png' height='20' />- Werkzaamheid Toevoegen Reparatie<br></li>
	</ul>
<p class='padding'>
</p>
</div>
</div>
</div>
<%@include file = "/footer.jsp" %>