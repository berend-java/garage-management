<%@include file = "/layout.jsp" %>
<%@ page import="com.b3r3nd.reference.Bedrijf" %>
<%@ page import="com.b3r3nd.domein.agenda.Monteur" %>
<%@ page import="java.util.ArrayList" %>

<%
Bedrijf b = (Bedrijf)getServletContext().getAttribute("Bedrijf");
ArrayList<Monteur> monteurs =  b.getWerkplaats().getDeMonteurs();
%>
<div class='title'>
    <h2>Monteur Koppelen</h2>
</div>
<div class='banner'>
    <table border='0'>
        <tr>
            <td width='380'></td>
            </tr>
    </table>
</div>
<div class='container'>
    <div class='tickets'>
		<form action='/Project/koppelmonteur' >
		<table border='0'>
			<tr><td>Selecteer Monteur</td><td><select name='monteur'>
			<%
			for(Monteur m: monteurs) {
				out.println("<option value='"+m.getUsername()+"'>"+ m.getVoornaam() +"</option>");
			}
			%>
			</select></td></tr>
			<tr><td collspan='2'><input title='Aanmaken' type='image' src='/Project/images/login.png' name='submit' value='Inloggen' /></td></tr>
		</table>
		<input type='hidden' name='reparatie' value='<%= request.getParameter("reparatie") %>'/>
		</form>
		<br><% if(request.getAttribute("fout") != null) {
				out.println(request.getAttribute("fout"));
			}
			%>
	</div>
</div>