<%@include file = "/layout.jsp" %>
<%@ page import="com.b3r3nd.reference.Bedrijf" %>
<%@ page import="com.b3r3nd.domein.garage.Auto" %>
<%@ page import="java.util.ArrayList" %>

<%
Bedrijf b = (Bedrijf)getServletContext().getAttribute("Bedrijf");
ArrayList<Auto> autos = b.getParkeergarage().getAutos();
%>
<div class='title'>
    <h2>Nieuwe Reparatie aanmaken</h2>
</div>
<div class='banner'>
    <table border='0'>
        <tr>
            <td width='380'></td>
            </tr>
    </table>
</div>
<div class='container'>
    <div class='tickets'>
		<form action='/Project/newreparatie' >
		<table border='0'>
			<tr><td width='150'>Naam: </td><td width='500'><input type='text' name='naam' value='${param.naam}'/></td></tr>
			<tr><td>Status: </td><td><input type='text' name='status' value='${param.status}'/> </td></tr>
			<tr><td>Auto</td><td><select name='auto'>
			<%
			for(Auto a: autos) {
				out.println("<option value='"+a.getKenteken()+"'>"+a.getType()+" " + a.getMerk()+"</option>");
			}
			%>
			</select></td></tr>
			<tr><td collspan='2'><input title='Aanmaken' type='image' src='/Project/images/login.png' name='submit' value='Inloggen' /></td></tr>
		</table></form>
		<br><% if(request.getAttribute("fout") != null) {
				out.println(request.getAttribute("fout"));
			}
			%>
	</div>
</div>