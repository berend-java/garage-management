<%@include file = "/layout.jsp" %>
<%@ page import="com.b3r3nd.domein.agenda.Agenda" %>
<%@ page import="com.b3r3nd.domein.agenda.Monteur" %>
<%@ page import="com.b3r3nd.reference.Bedrijf" %>
<%@ page import="java.util.ArrayList" %>
<div class='content'>

<div class='title'>
    <h1>Monteurs</h1>
</div>
<div class='container'>
	<table border='0'> 
		<tr class='headtable'>
			<td width='180'><b>Voornaam</b></td>
			<td width='200'><b>Achternaam</b></td>
			<td width='240'><b>Gevoortedatum</b></td>
			<td witdth="160"><b>Actions</b></td>
            </tr>
	</table>
<div id='tickets'>
<table border='0'>
<% Bedrijf b = (Bedrijf)getServletContext().getAttribute("Bedrijf");
	ArrayList<Monteur> monteurs = b.getWerkplaats().getDeMonteurs();
	int i = 0;

for (Monteur a: monteurs) {
	if(i == 0) { i++; }
	else if(i == 1) { i--; }
	out.println(" <tr class='table"+i+"'>" + 
            "<td width='194'>"+a.getVoornaam()+"</td>" +
            "<td width='200'>"+a.getAchternaam()+"</td>" +
            "<td width='220'>"+a.getGeboortedatum()+"</td>" +
            "<td witdth='160'><a href='/Project/secure/agenda/editmonteur.jsp?monteur="+a.getUsername()+"'><img title='Edit Monteur' src='/Project/images/edit.png' height='20'></a><a href='/Project/delmonteur?monteur="+a.getUsername()+"'><img title='Delete Monteur' src='/Project/images/delete.png' height='20'></a></td></tr>");
}
%>
</table>
	<ul>
		<li><a href='/Project/secure/agenda/newmonteur.jsp'><img title='Nieuwe Monteur' src='/Project/images/monteurs.png' height='20' />- Nieuwe Monteur</a><br></li>
		<li><img title='Edit Monteur' src='/Project/images/edit.png' height='20' />- Edit Monteur<br></li>
		<li><img title='Delete Monteur' src='/Project/images/delete.png' height='20' />- Delete Monteur<br></li>
	</ul>
<p class='padding'>
</p>
</div>
</div>
</div>
<%@include file = "/footer.jsp" %>