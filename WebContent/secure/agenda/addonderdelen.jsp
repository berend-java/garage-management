<%@include file = "/layout.jsp" %>
<%@ page import="com.b3r3nd.reference.Bedrijf" %>
<%@ page import="com.b3r3nd.domein.voorraad.Onderdeel" %>
<%@ page import="java.util.ArrayList" %>

<%
Bedrijf b = (Bedrijf)getServletContext().getAttribute("Bedrijf");
ArrayList<Onderdeel> onderdelen =  b.getVoorraadbeheer().getDeOnderdelen();
%>
<div class='title'>
    <h2>Onderdelen Toevoegen</h2>
</div>
<div class='banner'>
    <table border='0'>
        <tr>
            <td width='380'></td>
            </tr>
    </table>
</div>
<div class='container'>
    <div class='tickets'>
		<form action='/Project/addonderdelen' >
		<table border='0'>
			<tr><td>Selecteer Onderdeel</td><td><select name='onderdeel'>
			<%
			for(Onderdeel a: onderdelen) {
				out.println("<option value='"+a.getType()+"'>"+ a.getArtikelnummer() + " " + a.getType() + "</option>");
			}
			%>
			</select></td></tr>
			<tr><td>Aantal: </td><td><input type='text' name='aantal' value='${param.aantal}'/> </td></tr>
			<tr><td collspan='2'><input title='Aanmaken' type='image' src='/Project/images/login.png' name='submit' value='Inloggen' /></td></tr>
		</table>
		<input type='hidden' name='reparatie' value='<%= request.getParameter("reparatie") %>'/>
		</form>
		<br><% if(request.getAttribute("fout") != null) {
				out.println(request.getAttribute("fout"));
			}
			%>
	</div>
</div>