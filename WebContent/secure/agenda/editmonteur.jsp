<%@include file = "/layout.jsp" %>
<%@ page import="com.b3r3nd.reference.Bedrijf" %>
<%@ page import="com.b3r3nd.domein.garage.Auto" %>
<%@ page import="com.b3r3nd.domein.agenda.Monteur" %>
<%@ page import="java.util.ArrayList" %>

<%
Bedrijf b = (Bedrijf)getServletContext().getAttribute("Bedrijf");
Monteur m = b.getWerkplaats().zoekMonteur(request.getParameter("monteur"));
%>
<div class='title'>
    <h2>Edit Monteur</h2>
</div>
<div class='banner'>
    <table border='0'>
        <tr>
            <td width='380'></td>
            </tr>
    </table>
</div>
<div class='container'>
    <div class='tickets'>
		<form action='/Project/editmonteur' >
		<table border='0'>
			<tr><td width='150'>Voornaam: </td><td width='500'><input type='text' name='voornaam' value='<%= m.getVoornaam() %>'/></td></tr>
			<tr><td>Achternaam: </td><td><input type='text' name='achternaam' value='<%= m.getAchternaam() %>'/> </td></tr>
			<tr><td>Geboortedatum: </td><td><input type='text' name='geboortedatum' value='<%= m.getGeboortedatum() %>'/> </td></tr>
			<tr><td>Username: </td><td><input type='text' name='username' value='<%= m.getUsername() %>'/> </td></tr>
			<tr><td>password: </td><td><input type='text' name='password' value='<%= m.getPassword() %>'/> </td></tr>
			<tr><td collspan='2'><input title='Aanmaken' type='image' src='/Project/images/login.png' name='submit' value='Aanmaken' /></td></tr>
		</table>
		<input type='hidden' name='monteur' value='<%=request.getParameter("monteur") %>' /></form></form>
		<br><% if(request.getAttribute("fout") != null) {
				out.println(request.getAttribute("fout"));
			}
			%>
	</div>
</div>