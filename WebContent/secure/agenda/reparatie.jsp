<%@include file = "/layout.jsp" %>
<%@ page import="com.b3r3nd.domein.agenda.Reparatie" %>
<%@ page import="com.b3r3nd.reference.Bedrijf" %>
<%@ page import="com.b3r3nd.domein.klantenbinding.Klant" %>
<%@ page import="com.b3r3nd.domein.garage.Auto" %>
<%@ page import="com.b3r3nd.domein.agenda.Monteur" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.b3r3nd.domein.voorraad.Onderdeel" %>
<%@ page import="com.b3r3nd.domein.agenda.Werkzaamheid" %>
<%@ page import="com.b3r3nd.domein.agenda.Reparatie" %>
<%
Bedrijf b = (Bedrijf)getServletContext().getAttribute("Bedrijf");
ArrayList<Reparatie> reparaties = b.getWerkplaats().getDeReparaties();
String naam = request.getParameter("reparatie");
Reparatie rep = null;

for (Reparatie r: reparaties) {
	if(r.getNaam().equals(naam)) {
		rep = r; 
	}
} 
ArrayList<Monteur> monteurs = rep.getDeMonteurs();
%>
<div class='title'>
    <h2><% out.println(rep.getNaam()); %></h2>
</div>
<div class='banner'>
    <table border='0'>
        <tr>
            <td width='380'>Monteurs: <% for(Monteur m: monteurs) {
            	out.println(m.getUsername()); 
            	}%></td>
            </tr>
    </table>
</div>
<div class='container2'>
	<form action='/Project/editreparatie' ><input type='hidden' name='reparatie' value='<%=rep.getNaam() %>' />
    <div class='ti'>
            <table border='0'>
                <tr class='table3'>
                    <td width='150'>Status:</td>
                    <td width='270'><input type='text' name='status' value='<% out.println(rep.getStatus()); %>'/></td>
                </tr>
                <tr>
                    <td>BeginDatum:</td>
                    <td><% out.println(rep.getBegindatum()); %></td> 
                </tr>
                <tr class='table3'>
                    <td>EindDatum:</td>
                    <td><% out.println(rep.getEinddatum()); %></td>
                </tr>
                <tr>
                    <td>IsGereed:</td>
                    <td><input type='text' name='isgereed' value='<% out.println(rep.isGereed()); %>'/></td>
                </tr>
            </table>
    </div>
    <%
	Auto a = rep.getDeAuto();
    if(a == null) { System.out.println("nullll"); }
	Klant k = rep.getDeAuto().getDeEigenaar();
	%>
    <div class='klant'>
        <table border='0'>
            <tr class='table3'>
                <td width='200'>Voornaam:</td>
                <td width='200'><%=k.getVoornaam() %></td>
            </tr>
            <tr>
                <td>Achternaam:</td>
                <td><%=k.getAchternaam() %></td>
            </tr>
            <tr class='table3'>
                <td>Email:</td>
                <td><%=k.getEmail()  %></td>
            </tr>
            <tr>
                <td>Geboortedatum:</td>
                <td><%=k.getGeboortedatum()  %></td>
            </tr>
            <tr><td height='15'></td></tr><tr>
            <td><input title='Aanmaken' type='image' src='/Project/images/login.png' name='submit' value='Inloggen' /></form></td></tr>
        </table>
    </div>
    <div class='beschrijving'>
        <table border='0'>
            <tr>
                <td width='970' colspan='3' class='omschrijving'><h3>Auto Informatie</h3></td>
      		</tr>
            <tr class='table3'>
            	<td width='20'>Type:</td>
                <td width='150'><%=a.getType() %></td>
            </tr>
            <tr>
                <td>Merk:</td>
                <td><% out.println(a.getMerk()); %></td> 
            </tr>
            <tr class='table3'>
                <td>Brandstof:</td>
                <td><% out.println(a.getBrandstof()); %></td>
            </tr>
            <tr>
                <td>Kenteken:</td>
                <td><% out.println(a.getKenteken()); %></td>
            </tr>
            <tr>
                <td>Laatste Onderhoud:</td>
                <td><% out.println(a.getLaastOnderhoud()); %></td>
            </tr>
        </table>
    </div>
    <%
	ArrayList<Onderdeel> onderdelen = rep.getDeOnderdelen();
    ArrayList<Werkzaamheid> werkzaamheden = rep.getDeWerkzaamheden();
	%>
        <div class='files'>
        <table border='0'>
            <tr>
               <td width='970' colspan='3' class='omschrijving'><h3>Gebruikte Onderdelen</h3></td>
            </tr>
            <tr><td>Onderdeel</td><td>Aantal</td></tr>
            <% for (Onderdeel o1: onderdelen) { 
            	out.println("<tr> <td>" + o1.getType() +"</td> <td>"+ o1.getAantal() +" </td> </tr>");
	  }%>
        </table>
    </div>
     <div class='files'>
        <table border='0'>
            <tr>
               <td width='970' colspan='3' class='omschrijving'><h3>Huidige Werkzaamheden</h3></td>
            </tr>
            <tr><td>Omschrijving</td><td>Einddatum</td></tr>
            <% for (Werkzaamheid w1: werkzaamheden) { 
            	out.println("<tr> <td>" + w1.getOmschrijving() +"</td> <td>"+ w1.getEinddatum() +" </td> </tr>");
	  }%>
        </table>
    </div>
  </div>