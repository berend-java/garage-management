<%@include file = "/layout.jsp" %>
<%@ page import="com.b3r3nd.domein.agenda.Agenda" %>
<%@ page import="com.b3r3nd.domein.facturatie.Factuur" %>
<%@ page import="com.b3r3nd.reference.Bedrijf" %>
<%@ page import="java.util.ArrayList" %>
<div class='content'>

<div class='title'>
    <h1>Facturen</h1>
</div>
<div class='container'>
	<table border='0'> 
		<tr class='headtable'>
			<td width='230'><b>Datum</b></td>
			<td width='200'><b>betaald</b></td>
			<td width='200'><b>gereed</b></td>
			<td witdth="150"><b>Actions</b></td>
            </tr>
	</table>
<div id='tickets'>
<table border='0'>
<% Bedrijf b = (Bedrijf)getServletContext().getAttribute("Bedrijf");
	ArrayList<Factuur> facturen = b.getFacturatie().getFacturen();
	int i = 0;

for (Factuur f: facturen) {
	if(i == 0) { i++; }
	else if(i == 1) { i--; }
	out.println(" <tr class='table"+i+"'>" + 
            "<td width='150'>"+f.getDatum()+"</td>" +
            "<td width='120'>"+f.isBetaald()+"</td>" +
            "<td width='150'>"+f.isGereed()+"</td>" +
            "<td witdth='140'><td></td></tr>");
}
%>
</table>
	<ul>
	</ul>
<p class='padding'>
</p>
</div>
</div>
</div>
<%@include file = "/footer.jsp" %>