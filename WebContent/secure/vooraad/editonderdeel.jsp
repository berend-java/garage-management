<%@include file = "/layout.jsp" %>
<%@ page import="com.b3r3nd.reference.Bedrijf" %>
<%@ page import="com.b3r3nd.domein.garage.Auto" %>
<%@ page import="com.b3r3nd.domein.voorraad.Onderdeel" %>
<%@ page import="java.util.ArrayList" %>

<%
Bedrijf b = (Bedrijf)getServletContext().getAttribute("Bedrijf");
Onderdeel on = b.getVoorraadbeheer().zoekOnderdeel(request.getParameter("onderdeel"));
%>
<div class='title'>
    <h2>Edit Onderdeel</h2>
</div>
<div class='banner'>
    <table border='0'>
        <tr>
            <td width='380'></td>
            </tr>
    </table>
</div>
<div class='container'>
    <div class='tickets'>
		<form action='/Project/editonderdeel' >
		<table border='0'>
			<tr><td width='150'>Artikelnummer: </td><td width='500'><input type='text' name='artikelnummer' value='<%=on.getArtikelnummer() %>'/></td></tr>
			<tr><td>prijs: </td><td><input type='text' name='prijs' value='<%=on.getPrijs() %>'/> </td></tr>
			<tr><td>type: </td><td><input type='text' name='type' value='<%=on.getType() %>'/> </td></tr>

			<tr><td collspan='2'><input title='Aanmaken' type='image' src='/Project/images/login.png' name='submit' value='Aanmaken' /></td></tr>
		</table><input type='hidden' name='onderdeel' value='<%=on.getType() %>'/></form>
		<br><% if(request.getAttribute("fout") != null) {
				out.println(request.getAttribute("fout"));
			}
			%>
	</div>
</div>