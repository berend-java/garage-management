<%@include file = "/layout.jsp" %>
<%@ page import="com.b3r3nd.domein.agenda.Agenda" %>
<%@ page import="com.b3r3nd.domein.voorraad.Onderdeel" %>
<%@ page import="com.b3r3nd.reference.Bedrijf" %>
<%@ page import="java.util.ArrayList" %>
<div class='content'>

<div class='title'>
    <h1>Voorraad</h1>
</div>
<div class='container'>
	<table border='0'> 
		<tr class='headtable'>
			<td width='100'><b>Artikelnummer</b></td>
			<td width='100'><b>prijs</b></td>
			<td width='100'><b>aantal</b></td>
			<td width='140'><b>type</b></td>
			<td width='120'><b>Minimum Aantal</b></td>
			<td width="80"><b>Actions</b></td>
            </tr>
	</table>
<div id='tickets'>
<table border='0'>
<% Bedrijf b = (Bedrijf)getServletContext().getAttribute("Bedrijf");
	ArrayList<Onderdeel> onderdelen = b.getVoorraadbeheer().getDeOnderdelen();
	int ii = 0;

for (Onderdeel oo: onderdelen) {
	if(ii == 0) { ii++; }
	else if(ii == 1) { ii--; }
	String s33 = "Geen";
	String s22 = "<tr class='table"+ii+"'>";
	if(oo.getMinmumAantal() != 0) {
		s33 = Integer.toString(oo.getMinmumAantal());

		if(oo.getMinmumAantal() > oo.getAantal()) {
			s22 = "<tr class='red'>";
		}
	}
	out.println(s22);
	out.println("<td width='100'>"+oo.getArtikelnummer()+"</td>" +
            "<td width='100'>"+oo.getPrijs()+"</td>" +
            "<td width='100'>"+oo.getAantal()+"</td>" +
            "<td width='140'>"+oo.getType()+"</td>" +
            "<td width='100'>"+s33+"</td>" +
            "<td width='100'><a href='/Project/secure/vooraad/editonderdeel.jsp?onderdeel="+oo.getType()+"'><img title='Edit Onderdeel' src='/Project/images/edit.png' height='20'></a><a href='/Project/secure/vooraad/bestelonderdelen.jsp?onderdeel="+oo.getType()+"'><img title='Onderdelen Bestellen' src='/Project/images/addonderdelen.png' height='20'></a><a href='/Project/secure/vooraad/setminimum.jsp?onderdeel="+oo.getType()+"'><img title='Set Minimum' src='/Project/images/minimum.png' height='20'><a href='/Project/deleteonderdeel?onderdeel="+oo.getType()+"'><img title='Delete Onderdeel' src='/Project/images/delete.png' height='20'></a></td></tr>");
}
%>
</table>
	<ul>
		<li><a href='/Project/secure/vooraad/newonderdeel.jsp'><img title='Nieuw Onderdeel' src='/Project/images/voorraad.png' height='20' />- Nieuw Onderdeel</a><br></li>
		<li><img title='Onderdelen Bestellen' src='/Project/images/addonderdelen.png' height='20' />- Onderdelen Bestellen</a><br></li>
		<li><img title='Edit Onderdeel' src='/Project/images/edit.png' height='20' />- Edit Onderdeel</a><br></li>
		<li><img title='Set Minimum' src='/Project/images/minimum.png' height='20' />- Minimum Aantal Instellen</a><br></li>
		<li><img title='Delete Onderdeel' src='/Project/images/delete.png' height='20' />- Delete Onderdeel</a><br></li>
		<li> * Als het minimum aantal van een onderdeel is berijkt, zal deze rood opkleuren.
	</ul>
<p class='padding'>
</p>
</div>
</div>
</div>
<%@include file = "/footer.jsp" %>