<%@include file = "/layout.jsp" %>
<%@ page import="com.b3r3nd.reference.Bedrijf" %>
<%@ page import="com.b3r3nd.domein.garage.Auto" %>
<%@ page import="com.b3r3nd.domein.klantenbinding.Klant" %>
<%@ page import="java.util.ArrayList" %>

<%
Bedrijf b = (Bedrijf)getServletContext().getAttribute("Bedrijf");
Klant k = b.getKlantenbinding().zoekKlant(request.getParameter("klant"));
%>
<div class='title'>
    <h2>Edit Klant</h2>
</div>
<div class='banner'>
    <table border='0'>
        <tr>
            <td width='380'></td>
            </tr>
    </table>
</div>
<div class='container'>
    <div class='tickets'>
		<form action='/Project/editklant' >
		<table border='0'>
			<tr><td width='150'>Username: </td><td width='500'><input type='text' name='username' value='<%=k.getUsername() %>'/></td></tr>
			<tr><td>Password: </td><td><input type='text' name='password' value='<%=k.getPassword() %>'/> </td></tr>
			<tr><td>Voornaam: </td><td><input type='text' name='voornaam' value='<%=k.getVoornaam() %>'/> </td></tr>
			<tr><td>Achternaam: </td><td><input type='text' name='achternaam' value='<%=k.getAchternaam() %>'/> </td></tr>
			<tr><td>Email: </td><td><input type='text' name='email' value='<%=k.getEmail() %>'/> </td></tr>
			<tr><td collspan='2'><input title='Aanmaken' type='image' src='/Project/images/login.png' name='submit' value='Aanmaken' /></td></tr>
		</table>
		<input type='hidden' name='klant' value='<%=k.getUsername() %>' /></form>
		<br><% if(request.getAttribute("fout") != null) {
				out.println(request.getAttribute("fout"));
			}
			%>
	</div>
</div>