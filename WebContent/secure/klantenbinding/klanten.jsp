<%@include file = "/layout.jsp" %>
<%@ page import="com.b3r3nd.domein.agenda.Agenda" %>
<%@ page import="com.b3r3nd.domein.klantenbinding.Klant" %>
<%@ page import="com.b3r3nd.reference.Bedrijf" %>
<%@ page import="java.util.ArrayList" %>
<div class='content'>

<div class='title'>
    <h1>Klanten</h1>
</div>
<div class='container'>
	<table border='0'> 
		<tr class='headtable'>
			<td width='150'><b>Username</b></td>
			<td width='120'><b>Voornaam</b></td>
			<td width='150'><b>Achternaam</b></td>
			<td width='200'><b>Email</b></td>
			<td witdth="140"><b>Actions</b></td>
            </tr>
	</table>
<div id='tickets'>
<table border='0'>
<% Bedrijf b = (Bedrijf)getServletContext().getAttribute("Bedrijf");
	ArrayList<Klant> klanten = b.getKlantenbinding().getDeKlanten();
	int i = 0;

for (Klant r: klanten) {
	if(i == 0) { i++; }
	else if(i == 1) { i--; }
	out.println(" <tr class='table"+i+"'>" + 
            "<td width='150'>"+r.getUsername()+"</td>" +
            "<td width='120'>"+r.getVoornaam()+"</td>" +
            "<td width='150'>"+r.getAchternaam()+"</td>" +
            "<td width='180'>"+r.getEmail()+"</td>" +
            "<td witdth='140'><a href='/Project/secure/klantenbinding/klant.jsp?klant="+r.getUsername()+"'><img title='Edit Klant' src='/Project/images/edit.png' height='20'></a><a href='/Project/delklant?klant="+r.getUsername()+"'><img title='Delete Klant' src='/Project/images/delete.png' height='20'></a></tr>");
}
%>
</table>
	<ul>
		<li><a href='/Project/secure/klantenbinding/newklant.jsp'><img title='Nieuwe Klant' src='/Project/images/klant.png' height='20' />- Nieuwe Klant</a><br></li>
		<li><img title='Edit Klant' src='/Project/images/edit.png' height='20' />- Edit Klant<br></li>
		<li><img title='Delete Klant' src='/Project/images/delete.png' height='20' />- Delete Klant<br></li>
		<li><a href=''><img title='Herinner Klanten' src='/Project/images/factuur.png' height='20' />- Stuur Herinneringen</a><br></li>
	</ul>
<p class='padding'>
</p>
</div>
</div>
</div>
<%@include file = "/footer.jsp" %>