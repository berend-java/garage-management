<%@include file = "/layout.jsp" %>
<%@ page import="com.b3r3nd.reference.Bedrijf" %>
<%@ page import="com.b3r3nd.domein.garage.Auto" %>
<%@ page import="java.util.ArrayList" %>

<%
Bedrijf b = (Bedrijf)getServletContext().getAttribute("Bedrijf");
ArrayList<Auto> autos = b.getParkeergarage().getAutosZonderEigenaar();
%>
<div class='title'>
    <h2>Nieuwe Klant aanmaken</h2>
</div>
<div class='banner'>
    <table border='0'>
        <tr>
            <td width='380'></td>
            </tr>
    </table>
</div>
<div class='container'>
    <div class='tickets'>
		<form action='/Project/newklant' >
		<table border='0'>
			<tr><td width='150'>Username: </td><td width='500'><input type='text' name='username' value='${param.username}'/></td></tr>
			<tr><td>Password: </td><td><input type='text' name='password' value='${param.password}'/> </td></tr>
			<tr><td>Voornaam: </td><td><input type='text' name='voornaam' value='${param.voornaam}'/> </td></tr>
			<tr><td>Achternaam: </td><td><input type='text' name='achternaam' value='${param.achternaam}'/> </td></tr>
			<tr><td>Email: </td><td><input type='text' name='email' value='${param.email}'/> </td></tr>

			<tr><td collspan='2'><input title='Aanmaken' type='image' src='/Project/images/login.png' name='submit' value='Aanmaken' /></td></tr>
		</table></form>
		<br><% if(request.getAttribute("fout") != null) {
				out.println(request.getAttribute("fout"));
			}
			%>
	</div>
</div>