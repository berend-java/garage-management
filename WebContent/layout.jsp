<%@ page import="com.b3r3nd.user.Gebruiker" %>
<!DOCTYPE html>
<html>
<head>
    <title>ATD</title>
    <link href="/Project/index.css" rel="stylesheet" type="text/css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="/Project/js/jquery.js"></script>
</head>
<body>
    <div class='wrapper'>
        <div class='head'>
        <b class='loginfo'><img src="/Project/images/key.png" height='12'>
<% Object o = request.getSession().getAttribute("username");
	Object s = request.getSession().getAttribute("userrole");
	if(o != null && s != null) {
		out.println("Ingelogd als " + o + " ("+s+")- <a href='/Project/logout'>Logout</a>");
	}
%></b>
        </div>
        <header>
            <div id='logo'>
            	<img height='50' src="/Project/images/ATD.png" title="ATD" />
            </div>
            <div class='nav'><br>
                <ul>     
                    <li><a class='padding' href='/Project/secure/agenda/reparaties.jsp'><img title='Reparaties' src="/Project/images/reparatie.png" height='20'> - Reparaties</a></li>
                     <li><a class='padding' href='/Project/secure/agenda/werkzaamheden.jsp'><img title='Werkzaamheden' src="/Project/images/werkzaamheden.png" height='20'> - Werkzaamheden</a></li>
                    <li><a class='padding' href='/Project/secure/garage/autos.jsp'><img title='Autos' src="/Project/images/auto.png" height='20'> - Autos</a></li>
                    <li><a class='padding' href='/Project/secure/agenda/monteurs.jsp'><img title='Monteurs' src="/Project/images/monteurs.png" height='20'> - Monteurs</a></li>
                    <li><a class='padding' href='/Project/secure/facturatie/facturen.jsp'><img title='Facturen' src="/Project/images/factuur.png" height='20'> - Facturen</a></li>
                     <br><br><li><a class='padding' href='/Project/secure/klantenbinding/klanten.jsp'><img title='Klanten' src="/Project/images/klant.png" height='20'> - Klanten</a></li>
                    <li><a class='padding' href='/Project/secure/vooraad/vooraad.jsp'><img title='Voorraad' src="/Project/images/voorraad.png" height='20'> - Voorraad</a></li>
                    <li><a class='padding' href='/Project/secure/admin/users.jsp'><img title='Users' src="/Project/images/user.png" height='20'> - Users</a></li>
                    <li><a class='padding' href='/Project/secure/garage/parkeren.jsp'><img title='Perkeren' src="/Project/images/parking.png" height='20'> - Parkeren</a></li>
                </ul> 
            </div><br>
        </header>
